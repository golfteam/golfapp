﻿using Android.App;
using Android.Content;
using Android.Provider;
using Android;
using Android.OS;
using Core.BL.Geolocalisation;

namespace GolfApp.Android.Implementations
{
	public class Device : IDevice
	{
		public Device(Activity context)
		{
			Context = context;
		}

		private Context Context { get; set; }

		public string Name
		{
			get { return Build.Product; }
		}

		public string MonoMobileVersion
		{
			get { return "0.1.0"; }
		}

		public string Platform
		{
			get { return "Android";  }
		}

		public string UUID
		{
			get { return Settings.Secure.GetString(Context.ContentResolver, Settings.Secure.AndroidId); }
		}

		public string Version
		{
			get { return Build.VERSION.Release; }
		}
	}
}