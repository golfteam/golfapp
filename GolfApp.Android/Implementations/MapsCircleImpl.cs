﻿using Android.Gms.Maps.Model;
using Core.BL.Geolocalisation;
using GolfApp.Core.SAL;

namespace GolfApp.Android
{

    public class MapsCircleImpl : IPlatformDependantMapsCircle
    {
        private Circle _circle;
        public Circle circle { get { return _circle; } }

		/// <summary>
		/// Initializes a new instance of the <see cref="GolfApp.Android.MapsCircleImpl"/> class.
		/// </summary>
		/// <param name="c">C.</param>
        public MapsCircleImpl(Circle c)
        {
            _circle = c;
        }

		/// <summary>
		/// Place le centre du cercle à l'emplacement voulu
		/// </summary>
		/// <param name="l">L'emplacement où placer le centre</param>
        public void setCenter(Position l)
        {
            circle.Center = new LatLng(l.Latitude, l.Longitude);
        }

		/// <summary>
		/// Définit le rayon du cercle
		/// </summary>
		/// <param name="r">Rayon du cercle</param>
        public void setRadius(double r)
        {
            circle.Radius = r;
        }

		/// <summary>
		/// Rend l'emplacement du centre du cercle
		/// </summary>
		/// <returns>Le centre du cercle</returns>
        public Position getCenter()
        {
            return new Position(circle.Center.Latitude, circle.Center.Longitude);
        }

		/// <summary>
		/// Rend le rayon du cercle
		/// </summary>
		/// <returns>Le rayon du cercle</returns>
        public double getRadius()
        {
            return circle.Radius;
        }
    }
}

