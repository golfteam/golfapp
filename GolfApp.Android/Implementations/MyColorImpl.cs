﻿using Android.Graphics;
using GolfApp.Core.BL;
using System;

namespace GolfApp.Android
{
    class MyColorImpl : MyColor
    {
		/// <summary>
		/// Rend la couleur au format Android
		/// </summary>
		/// <returns>The color.</returns>
        Color getColor()
        {
            return new Color(this.Red, this.Green, this.Blue, this.Alpha);
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="GolfApp.Android.MyColorImpl"/> class.
		/// </summary>
		/// <param name="red">Red.</param>
		/// <param name="green">Green.</param>
		/// <param name="blue">Blue.</param>
		/// <param name="alpha">Alpha.</param>
        public MyColorImpl(Byte red, Byte green, Byte blue, Byte alpha)
            : base(red, green, blue, alpha)
        {

        }

    }
}

