﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.Gms.Maps;
using GolfApp.Core.SAL;
using Android.Gms.Maps.Model;
using GolfApp.Core.BL;
using Android.Graphics;
using Core.BL.Geolocalisation;

namespace GolfApp.Android.Implementations
{
	class GoogleMapImpl : IPlatformDependantGoogleMap
	{


		public GoogleMap map { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="GolfApp.Android.Implementations.GoogleMapImpl"/> class.
		/// </summary>
		/// <param name="mapFrag">Map frag.</param>
		public GoogleMapImpl(MapFragment mapFrag){
			map = mapFrag.Map;
		}

		/// <summary>
		/// Initialise la carte avec les paramètres fournis et une vue aérienne
		/// </summary>
		/// <param name="defautPosition">Defaut position.</param>
		/// <param name="ZoomControlEnabled">If set to <c>true</c> zoom control enabled.</param>
		/// <param name="CompassEnabled">If set to <c>true</c> compass enabled.</param>
        public void init(Position defautPosition, bool ZoomControlEnabled, bool CompassEnabled)
        {
			// Dans le cas où la position n'est pas encore définie, se placer à (0,0)
			if (defautPosition == null)
				defautPosition = new Position (0, 0);

            map.MoveCamera(CameraUpdateFactory.NewLatLngZoom(toLatLng(defautPosition), 16));
            map.UiSettings.ZoomControlsEnabled = ZoomControlEnabled;
            map.UiSettings.CompassEnabled = CompassEnabled;
			map.MapType = GoogleMap.MapTypeSatellite;
        }

		/// <summary>
		/// Ajoute le cercle.
		/// </summary>
		/// <returns>The cercle.</returns>
		/// <param name="centre">Centre.</param>
		/// <param name="rayon">Rayon.</param>
		/// <param name="couleurDisque">Couleur disque.</param>
		/// <param name="tailleBordure">Taille bordure.</param>
		/// <param name="couleurBordure">Couleur bordure.</param>
		IPlatformDependantMapsCircle IPlatformDependantGoogleMap.ajouterCercle(Position centre, double rayon, MyColor couleurDisque, int tailleBordure, MyColor couleurBordure)
		{
			CircleOptions circleOptions = new CircleOptions ();
			circleOptions.InvokeCenter (toLatLng(centre));
			circleOptions.InvokeRadius (rayon);
			circleOptions.InvokeFillColor(couleurDisque.toARGB());
			circleOptions.InvokeStrokeWidth (6);
			circleOptions.InvokeStrokeColor (couleurDisque.toARGB());

			return new MapsCircleImpl (map.AddCircle (circleOptions));
		}

		/// <summary>
		/// Ajouters the marqueur.
		/// </summary>
		/// <returns>The marqueur.</returns>
		/// <param name="l">L.</param>
		/// <param name="draggable">If set to <c>true</c> draggable.</param>
		/// <param name="couleur">Couleur.</param>
		IPlatformDependantMapsMarker IPlatformDependantGoogleMap.ajouterMarqueur(Position l, bool draggable, MyColor couleur)
		{
			MarkerOptions markerOpt = new MarkerOptions();
			markerOpt.SetPosition (toLatLng(l));
			markerOpt.Draggable(draggable);
			markerOpt.InvokeIcon(BitmapDescriptorFactory.DefaultMarker (couleur.toHUE()));
			return new MapsMarkerImpl (map.AddMarker (markerOpt));
		}

		/// <summary>
		/// Convertit une localisation en object LatLng
		/// </summary>
		/// <returns>The lat lng.</returns>
		/// <param name="l">La localisation</param>
		public static LatLng toLatLng(Position l){
			return new LatLng (l.Latitude, l.Longitude);
		}

		/// <summary>
		/// Retire un marqueur sur la carte
		/// </summary>
		/// <param name="marker">Le marqueur à retirer</param>
		/// <returns>Vrai si l'opération s'est bien déroulée</returns>
		/// <param name="cercle">Cercle.</param>
		public void retirerCercle (IPlatformDependantMapsCircle cercle)
		{

		}

		/// <summary>
		/// Retire un marqueur sur la carte
		/// </summary>
		/// <param name="marker">Le marqueur à retirer</param>
		/// <returns>Vrai si l'opération s'est bien déroulée</returns>
		public void retirerMarqueur (IPlatformDependantMapsMarker marker)
		{

		}
       
    }
}
