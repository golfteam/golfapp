﻿using Android.Gms.Maps.Model;
using Core.BL.Geolocalisation;
using GolfApp.Core.SAL;

namespace GolfApp.Android
{
	class MapsMarkerImpl : IPlatformDependantMapsMarker
    {
        public Marker marker;

        public MapsMarkerImpl(Marker marker)
        {
            this.marker = marker;
        }

        public Position getCoord()
        {
            return new Position(marker.Position.Latitude, marker.Position.Longitude);
        }

        public void setCoord(Position l)
        {
            marker.Position = toLatLng(l);
        }

        public bool isDraggable()
        {
            return marker.Draggable;
        }

        public void setDraggable(bool draggable)
        {
            marker.Draggable = draggable;
        }

        /// <summary>
        /// Convertit une localisation en object LatLng
        /// </summary>
        /// <returns>The lat lng.</returns>
        /// <param name="l">La localisation</param>
        public static LatLng toLatLng(Position l)
        {
            return new LatLng(l.Latitude, l.Longitude);
        }

		/// <summary>
		/// Convertit un objet LatLng en un objet Position
		/// </summary>
		/// <returns>The position.</returns>
		/// <param name="l">Le LatLng</param>
		public static Position toPosition(LatLng l)
		{
			return new Position (l.Latitude, l.Longitude);

		}

		/// <summary>
		/// Crée ou modifie la fenêtre d'info qui affiche la distance
		/// </summary>
		/// <param name="p">La position de départ</param>
		public void setInfoWindowsObjectif (Position p)
		{
			marker.Title = "Objectif";
			marker.Snippet = "Distance : " + p.distance(toPosition(marker.Position)).ToString("F") + " mètres";
			marker.ShowInfoWindow ();
		}

		public void setInfoWindowsObjectif ()
		{
			marker.Title = "Objectif";
			marker.ShowInfoWindow ();
		}
			
		/// <summary>
		/// Crée la bulle d'info qui indique que le marqueur est la position
		/// </summary>
		public void setInfoWindowsPosition ()
		{
			marker.Title = "Position";
			marker.ShowInfoWindow ();
		}

		public void setInfoWindowsArrivee ()
		{
			marker.Title = "Arrivée";
			marker.ShowInfoWindow ();
		}
    }
}

