﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GolfApp.Core;
using Android.Locations;

namespace GolfApp.Android
{
	[Activity (Label = "INSA GolfApp", MainLauncher = true)]			
	public class MainActivity : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			locMgr = GetSystemService (Context.LocationService) as LocationManager;

			Button startGameButton =  FindViewById<Button>(Resource.Id.startGame);
			Button myStatsButton = FindViewById<Button> (Resource.Id.MyStats);
			Button helpButton = FindViewById<Button> (Resource.Id.help);

			manager = new Manager (Application.GetExternalFilesDir (null).AbsolutePath, Manager.System.ANDROID);

			startGameButton.Click += delegate {
				StartActivity(typeof(startGame));
			};
			myStatsButton.Click += delegate {
				StartActivity(typeof(myStats));
			};
			helpButton.Click += delegate {
				StartActivity(typeof(help));
			};



			// Create your application here
		}
	}
}

