﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GolfApp.Core.BL;
using GolfApp.Core;
using BarChart;
using System.Xml;

namespace GolfApp.Android
{
	[Activity (Label = "Statistiques")]	
	public class graphique : MyActivity
	{
		BarChartView graph;
		String choixStat;
		Club.typeClub choixClub;
		float [] t1 = new float[1];

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.myStats);

			// Initialisation des attributs pour faire la bonne requete sur le xml
			graph =  FindViewById<BarChartView>(Resource.Id.barChart);
			choixStat = Intent.GetStringExtra ("nomStat");
			choixClub = manager.ferStatChoisi(Intent.GetStringExtra ("nomClub"));
			float nbTirs;
			float[] tabItems = new float[0];
			TextView TVNbTirs = FindViewById<TextView> (Resource.Id.TVNbTirsStats);
			Button retour = FindViewById<Button> (Resource.Id.ButtonRetourMenuStats);
			retour.Click += delegate {
				StartActivity(typeof(myStats));
			};

			try{
				switch (choixStat) {
				case "Déviation":
					tabItems = manager.recupererDeviation (choixClub); 
					graph.ItemsSource = Array.ConvertAll (tabItems, v => new BarModel {
						Value = v,
							Legend = manager.objStats.getLegendDeviation ()
					});
					graph.AutoLevelsEnabled = false;
					manager.initStats ();
					break;
					
				case "Distance à l\'objectif":
					tabItems = manager.recupererDistanceObjectif (choixClub);
					graph.ItemsSource = Array.ConvertAll (tabItems, v => new BarModel {
							Value = v,
							Legend = manager.objStats.getLegendDistance ()
						});
						graph.AutoLevelsEnabled = false;
						manager.initStats ();
						break;

				case "Distance":
					tabItems = manager.recupererDistanceClub (choixClub);
					graph.ItemsSource = Array.ConvertAll (tabItems, v => new BarModel {
						Value = v,
						Legend = manager.objStats.getLegendClubDistance ()
					});
					graph.AutoLevelsEnabled = false;
					manager.initStats ();
					break;

				default :
					break;
				}
				nbTirs = tabItems.Sum ();

				if (nbTirs == 0) {
					TVNbTirs.Text = "Aucun tir correspondant à la sélection.";
				} else {
					TVNbTirs.Text = "Calculé avec " + nbTirs + " tirs enregistrés.";
				}

			}catch(XmlException){
				// se produit si le fichier xml n'a pas encore été créé ou a un format incorrect
				TVNbTirs.Text = "Erreur lors de la récupération des tirs";
			}


		}

	}
}

