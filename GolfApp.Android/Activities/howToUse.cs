﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GolfApp.Android
{
	[Activity (Label = "Comment utiliser ?")]			
	public class howToUse : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.HowToUse);

			Button deroulementButton = FindViewById<Button> (Resource.Id.deroulement);
			deroulementButton.Click += delegate {
				StartActivity(typeof(helpDeroulement));
			};
			Button statsButton = FindViewById<Button> (Resource.Id.stats);
			statsButton.Click += delegate {
				StartActivity(typeof(helpStats));
			};
		}
	}
}

