﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GolfApp.Core;

namespace GolfApp.Android
{
	[Activity (Label = "En attente du tir")]			
	public class waitForShot : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			manager.etapeEnCours = Manager.Etape.waitingForShot;

			base.OnCreate (bundle);

			// Create your application here
		}
	}
}

