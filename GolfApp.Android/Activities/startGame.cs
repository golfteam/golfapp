﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GolfApp.Core;
using Android.Locations;
using Android.Util;
using Java.Lang;

namespace GolfApp.Android
{
	[Activity (Label = "Démarrer la partie")]			
	public class startGame : MyActivity
	{

		EditText par;
		protected override void OnCreate (Bundle bundle)
		{
			manager.etapeEnCours = Manager.Etape.startGame;


			base.OnCreate (bundle);

			Criteria locationCriteria = new Criteria ();
			locationCriteria.Accuracy = Accuracy.Fine;
			locationCriteria.PowerRequirement = Power.NoRequirement;


			string provider = locMgr.GetBestProvider(locationCriteria, true);

			// Recherche de la dernière position connue
			var p = locMgr.GetLastKnownLocation (provider);
			if (p != null) {
				manager.position = locationToPosition(p);
			}

			if(locMgr.IsProviderEnabled(provider))
			{
				// Activation de la mise à jour de la géolocalisation quand un changement de position est détecté
				locMgr.RequestLocationUpdates (provider, 2000, 1, this);
			} 
			else 
			{
				Log.Info("Localisation",provider + " is not available. Does the device have location services enabled?");
			}
		
			SetContentView (Resource.Layout.startGame);
			// Create your application here

			par = FindViewById<EditText> (Resource.Id.TFPar);

			Button startGame = FindViewById<Button> (Resource.Id.buttonStartHole);

			startGame.Click += delegate {
				int parTemp;
				if(int.TryParse(par.Text, out parTemp)){
					manager.par=parTemp;
					StartActivity(typeof(DefineObjective));
				} else {
					Toast.MakeText (this, "Définissez une valeur au par avant de continuer", ToastLength.Long).Show();
				}
			};
		}
	}
}

