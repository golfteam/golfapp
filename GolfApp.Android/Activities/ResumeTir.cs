﻿using Android.App;
using Android.OS;
using Android.Widget;
using GolfApp.Core.BL;
using GolfApp.Android.Implementations;
using Android.Gms.Maps;

namespace GolfApp.Android
{
	[Activity(Label = "Résumé du tir")]
    public class ResumeTir : MyActivity
    {
        Button enregistrer;
        Button nePasEnregistrer;
        TextView TVResumeTir;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ResumeTir);
            // Create your application here
            enregistrer = FindViewById<Button>(Resource.Id.BtnEnregistrer);
            nePasEnregistrer = FindViewById<Button>(Resource.Id.BtnNePasEnregistrer);
            TVResumeTir = FindViewById<TextView>(Resource.Id.TVResumeTir);

            Tir tirEnCours = manager.tirEnCours;
            TVResumeTir.Text = "Distance à l'objectif : " + tirEnCours.distanceDepartObjectif().ToString("F") + "m" +
                "\nDistance réelle : " + tirEnCours.distanceDepartArrivee().ToString("F") + "m" +
                "\nEcart de distance : " + tirEnCours.diffDistance().ToString("F") + "m" +
                "\nSoit " + tirEnCours.ecartDistance().ToString("F") + " %" +
                "\nAngle de déviation " + tirEnCours.angleDeviation().ToString("F") + "°";

			GoogleMapImpl map2 = new GoogleMapImpl((MapFragment)FragmentManager.FindFragmentById(Resource.Id.map2));

			manager.carteResumeTir(map2);


            enregistrer.Click += delegate
            {

                manager.enregistrerTir();
                // Affichage d'un message lorsque le tir est enregistré
                afficherMessageTirEnregistre();
                manager.Clear();
                StartActivity(typeof(DefineObjective));
            };
            nePasEnregistrer.Click += delegate
            {
                manager.Clear();
                StartActivity(typeof(DefineObjective));
            };
        }

        /// <summary>
        /// Affiche un message indiquant que le tir est correctement enregistré
        /// </summary>
        public void afficherMessageTirEnregistre()
        {
            Toast.MakeText(this, "Tir enregistré", ToastLength.Long).Show();
        }


    }
}

