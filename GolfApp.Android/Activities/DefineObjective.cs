﻿using Android.App;
using Android.Gms.Maps;
using Android.OS;
using Android.Widget;
using Core.BL.Geolocalisation;
using GolfApp.Android.Implementations;
using GolfApp.Core;

namespace GolfApp.Android
{
	[Activity(Label = "Définir l'objectif")]
    public class DefineObjective : MyActivity, GoogleMap.IOnMapClickListener
    {
        Button valider;
		Button retour;
        protected override void OnCreate(Bundle bundle)
        {
            manager.etapeEnCours = Manager.Etape.defineObjective;

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DefineObjective);

            valider = FindViewById<Button>(Resource.Id.ButtonDefineObjective);
			retour = FindViewById<Button> (Resource.Id.BoutonRetourMenu);


			retour.Click += delegate {
				StartActivity(typeof(MainActivity));
			};
            valider.Click += delegate { onValidateClick(); };
			GoogleMapImpl map = new GoogleMapImpl((MapFragment)FragmentManager.FindFragmentById(Resource.Id.map));
            map.map.SetOnMapClickListener(this);
            manager.startGame(map);

        }

		public override void  OnBackPressed(){
			StartActivity(typeof(MainActivity));
		}
        void GoogleMap.IOnMapClickListener.OnMapClick(global::Android.Gms.Maps.Model.LatLng p0)
        {
            Position p = new Position(p0.Latitude, p0.Longitude);
            manager.onMapClick(p);
        }

        void onValidateClick()
        {
            if (manager.isObjectiveDefined())
            {
                StartActivity(typeof(ChoixFer));
            }
            else
            {
                Toast.MakeText(this, "Définissez un objectif avant de continuer", ToastLength.Long).Show();
            }
        }

		protected override void OnRestoreInstanceState(Bundle savedInstanceState){
			base.OnRestoreInstanceState (savedInstanceState);
			manager.recreerObjectif ();
		}


    }
}

