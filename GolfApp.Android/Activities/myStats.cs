﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using GolfApp.Core.BL;
using GolfApp.Core;
using BarChart;

namespace GolfApp.Android
{
	[Activity (Label = "Mes statistiques")]			
	public class myStats : MyActivity
	{
		Spinner spinnerChoixStat;
		Spinner spinnerChoixFer;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Stats);
			spinnerChoixStat = FindViewById<Spinner>(Resource.Id.SpinnerChoixStat);

			spinnerChoixStat.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_StatSelected);

			var adapter2 = ArrayAdapter.CreateFromResource(
				this, Resource.Array.stats_array, Android.Resource.Layout.SimpleSpinnerItem);

			adapter2.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerChoixStat.Adapter = adapter2;

			spinnerChoixFer = FindViewById<Spinner>(Resource.Id.SpinnerChoixFerStats);

			spinnerChoixFer.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_FerSelected);

			var adapter1 = ArrayAdapter.CreateFromResource(
				this, Resource.Array.clubs_stat_array, Android.Resource.Layout.SimpleSpinnerItem);

			adapter1.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerChoixFer.Adapter = adapter1;

			spinnerChoixFer.OnItemSelectedListener = new OnItemSelectedListenerImpl ();

			spinnerChoixStat.OnItemSelectedListener = new OnItemSelectedListenerImpl ();
		
			Button showStats =  FindViewById<Button>(Resource.Id.showStats);

			showStats.Click += delegate {
				var nouvelleActivity = new Intent (this, typeof(graphique));
				nouvelleActivity.PutExtra("nomStat",spinnerChoixStat.SelectedItem.ToString());
				nouvelleActivity.PutExtra("nomClub", spinnerChoixFer.SelectedItem.ToString());
				StartActivity(nouvelleActivity);
			};
		
			Button retMenu = FindViewById<Button> (Resource.Id.retMenu);
			retMenu.Click += delegate {
				StartActivity(typeof(MainActivity));
			};
		}

			

		private void spinner_FerSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}

		private void spinner_StatSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}



		internal class OnItemSelectedListenerImpl : myStats, AdapterView.IOnItemSelectedListener  {

			#region IOnItemSelectedListener implementation
			public void OnItemSelected (AdapterView parent, View view, int position, long id){

			}


			public void OnNothingSelected (AdapterView parent) {
			} 
			#endregion 
		}

	}
}

