﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GolfApp.Android
{
	[Activity (Label = "Règles")]			
	public class golfRules : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.GolfRulesMenu);
			//Button backButton = FindViewById<Button> (Resource.Id.back);
			Button courseButton = FindViewById<Button> (Resource.Id.course);
			Button matchButton = FindViewById<Button> (Resource.Id.game);

			/*backButton.Click += delegate {
				StartActivity(typeof(help));
			};*/
			courseButton.Click += delegate {
				StartActivity(typeof(golfRulesCourse));
			};
			matchButton.Click += delegate {
				StartActivity(typeof(golfRulesGame));
			};

		}
	}
}

