﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GolfApp.Android
{
	[Activity (Label = "Aide")]			
	public class help : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.HelpMenu);

			Button golfRulesButton =  FindViewById<Button>(Resource.Id.golfRules);
			Button howToUseButton = FindViewById<Button> (Resource.Id.howToUse);
			Button backButton = FindViewById<Button> (Resource.Id.back);
		
			golfRulesButton.Click += delegate {
				StartActivity(typeof(golfRules));
			};
			howToUseButton.Click += delegate {
				StartActivity(typeof(howToUse));
			};
			backButton.Click += delegate {
				StartActivity(typeof(MainActivity));
			};
		}
	}
}

