﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using GolfApp.Core;
using System;

namespace GolfApp.Android
{
	[Activity(Label = "Choix du club")]
    public class ChoixFer : MyActivity
    {
        Button valider;
        /// <summary>
        /// Indique si l'activité est en attente du tir
        /// </summary>
        Spinner spinner;
        TextView textview;
        protected override void OnCreate(Bundle bundle)
        {
            manager.etapeEnCours = Manager.Etape.choixFer;

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ChoixFer);

			valider = FindViewById<Button>(Resource.Id.BoutonValidationChoixFer);
            textview = FindViewById<TextView>(Resource.Id.TVSpinner);


            valider.Click += delegate { onValidateClick(); };

            spinner = FindViewById<Spinner>(Resource.Id.SpinnerChoixFer);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);

            var adapter = ArrayAdapter.CreateFromResource(
                this, Resource.Array.clubs_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

        }

        void onValidateClick()
        {
            if (manager.etapeEnCours != Manager.Etape.waitingForShot)
            {
                manager.ferChoisi(spinner.SelectedItem.ToString());
                manager.etapeEnCours = Manager.Etape.waitingForShot;
                spinner.Visibility = ViewStates.Invisible;
                textview.Text = "En attente du tir...\nPlacez-vous sur le point d'arrivée de la balle avant d'appuyer sur valider.";
            }
            else
            {
                manager.ajouterArrivee();
                StartActivity(typeof(ResumeTir));
            }
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {

        }
    }
}

