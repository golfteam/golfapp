﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BarChart;
using GolfApp.Core.BL;
using System.Threading.Tasks;
using System.IO;


namespace GolfApp.Android
{
	[Activity (Label = "statsDistance")]			
	public class statsDistance : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			float[] t1 = manager.recupererStats ();
			base.OnCreate (bundle);
			// Données des barres (taille de chacune placées dans un tableau)
			var chart = new BarChartView (this) {
				ItemsSource = Array.ConvertAll (t1, v => new BarModel { Value = v , Legend = manager.objStats.getLegendDistance()})
			};


			// On enlève l'échelle des axes par défaut
			//chart.AutoLevelsEnabled = false;

			// On rajoute un indice pour le maximum
			//chart.AddLevelIndicator (33f, title: "Freq");

			AddContentView (chart, new ViewGroup.LayoutParams (
				ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent));
		}
	}
}

