﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BarChart;
using GolfApp.Core;
using GolfApp.Core.BL;

namespace GolfApp.Android
{
	[Activity (Label = "statsDeviation")]			
	public class statsDeviation : MyActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Stats obj = new Stats ();

			base.OnCreate (bundle);
			// Données des barres (taille de chacune placées dans un tableau)
			var data = new [] { 10f, 22f, 14f, 8f, 6f, 2f };
			var chart = new BarChartView (this) {
				ItemsSource = Array.ConvertAll (data, v => new BarModel { Value = v , Legend = obj.getLegendDeviation() })
			};


			// On enlève l'échelle des axes par défaut
			chart.AutoLevelsEnabled = false;

			// On rajoute un indice pour le maximum
			chart.AddLevelIndicator (22f, title: "Freq");

			AddContentView (chart, new ViewGroup.LayoutParams (
				ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent));
		
		}
	}
}

