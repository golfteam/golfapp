﻿using Android.App;
using Android.Locations;
using Android.OS;
using Core.BL.Geolocalisation;
using GolfApp.Core;

namespace GolfApp.Android
{
    public class MyActivity : Activity, ILocationListener
    {
		protected static Manager manager;
        protected static LocationManager locMgr;


        void ILocationListener.OnLocationChanged(Location location)
        {
            Position p = new Position(location.Latitude, location.Longitude);
            p.Accuracy = location.Accuracy;
            p.Altitude = location.Altitude;
            manager.OnLocationChanged(p);
        }

        void ILocationListener.OnProviderDisabled(string provider)
        {

        }

        void ILocationListener.OnProviderEnabled(string provider)
        {

        }

        void ILocationListener.OnStatusChanged(string provider, Availability status, Bundle extras)
        {

        }

        public Position locationToPosition(Location l)
        {
            Position p = new Position(l.Latitude, l.Longitude);
            p.Altitude = l.Altitude;
            p.Accuracy = l.Accuracy;
            return p;
        }
    }
}

