﻿using GolfApp.Core.BL;
using System;
using System.Xml.Serialization;

namespace GolfApp.Core.DAL
{

    /// <summary>
    /// Classe de stockage des paramètres
    /// </summary>
    public class Parametres
    {
        [XmlElement("MapCompassEnabled")]
        public Boolean MapCompassEnabled { get; set; }
        [XmlElement("MapZoomControlEnabled")]
        public Boolean MapZoomControlEnabled { get; set; }
        [XmlElement("DesiredAccuracy")]
        public double DesiredAccuracy { get; set; }
        [XmlElement("GeolocationTimeout")]
        public int GeolocationTimeout { get; set; }
        [XmlElement("CouleurMarqueurPosition")]
        public MyColor CouleurMarqueurPosition { get; set; }
        [XmlElement("DraggableMarqueurPosition")]
        public bool DraggableMarqueurPosition { get; set; }
        [XmlElement("CouleurCerclePosition")]
        public MyColor CouleurCerclePosition { get; set; }
        [XmlElement("CouleurBordureCerclePosition")]
        public MyColor CouleurBordureCerclePosition { get; set; }
        [XmlElement("tailleBordureCerclePosition")]
        public int tailleBordureCerclePosition { get; set; }
        [XmlElement("CouleurMarqueurObjectif")]
        public MyColor CouleurMarqueurObjectif { get; set; }
        [XmlElement("DraggableMarqueurObjectif")]
        public Boolean DraggableMarqueurObjectif { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GolfApp.Core.DAL.Parametres"/> class with default parameters
        /// </summary>
        public Parametres()
        {
            MapCompassEnabled = true;
            MapZoomControlEnabled = true;
            DesiredAccuracy = 1;
            GeolocationTimeout = 1000;
            CouleurMarqueurPosition = new MyColor(200, 12, 12, 255);
            DraggableMarqueurPosition = false;
			CouleurCerclePosition = new MyColor(10, 10, 200, 50);
			CouleurBordureCerclePosition = new MyColor(10, 10, 200, 120);
            tailleBordureCerclePosition = 9;
            CouleurMarqueurObjectif = new MyColor(200, 10, 10, 255);
            DraggableMarqueurObjectif = false;
        }



    }
}
