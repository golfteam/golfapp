﻿using Core.BL.Geolocalisation;
using PCLStorage;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace GolfApp.Core.BL
{
    public class Tir
    {
        #region Enumérations
        public enum DirectionDeviation
        {
            GAUCHE,
            DROITE,
            NONE
        }
        #endregion

        #region Constantes

        public const string XMLNODE_TIR = "tir";
        public const string XMLNODE_TIRS = "tirs";
        public const string XMLNODE_LATITUDE = "latitude";
        public const string XMLNODE_LONGITUDE = "longitude";
        public const string XMLNODE_OBJECTIF = "objectif";
        public const string XMLNODE_DEPART = "depart";
        public const string XMLNODE_ARRIVEE = "arrivee";
        public const string FILENAME_TIRS_XML = "tirs.xml";
        public const string XMLNODE_CLUB = "club";
        public const string XMLNODE_DISTREELLE = "distReelle";
        public const string XMLNODE_DISTOBJECTIF = "distObjectif";
        public const string XMLNODE_ANGLE = "angle";
        public const string XMLNODE_DIRECTION = "direction";
        public const string XMLNODE_ECARTOBJECTIF = "ecartObjectif";

        #endregion

        #region Propriétés
		/// <summary>
		/// Gets or sets the depart.
		/// </summary>
		/// <value>The depart.</value>
        public Position depart { get; set; }

		/// <summary>
		/// Gets or sets the objectif.
		/// </summary>
		/// <value>The objectif.</value>
        public Position objectif { get; set; }

		/// <summary>
		/// Gets or sets the arrivee.
		/// </summary>
		/// <value>The arrivee.</value>
        public Position arrivee { get; set; }

		/// <summary>
		/// Gets or sets the club.
		/// </summary>
		/// <value>The club.</value>
        public Club club { get; set; }

        #endregion

        #region Constructeur
		/// <summary>
		/// Initializes a new instance of the <see cref="GolfApp.Core.BL.Tir"/> class.
		/// </summary>
        public Tir()
        {

        }
        #endregion

        /// <summary>
        /// Distance entre le point de départ et l'objectif
        /// </summary>
        /// <returns>The depart objectif.</returns>
        public double distanceDepartObjectif()
        {
            return depart.distance(objectif);
        }

        /// <summary>
        /// Distance entre le point de départ et le point d'arrivée réel
        /// </summary>
        /// <returns>The depart arrivee.</returns>
        public double distanceDepartArrivee()
        {
            return depart.distance(arrivee);
        }

        /// <summary>
        /// Distance entre l'arrivée du tir réel et son objectif
        /// </summary>
        /// <returns>The arrivee objectif.</returns>
        public double distanceArriveeObjectif()
        {
            return arrivee.distance(objectif);
        }

        /// <summary>
        /// Rend l'angle entre l'objectif et le tir réel (en °)
        /// </summary>
        /// <returns>L'angle de déviation.</returns>
        public double angleDeviation()
        {
            return bearing(depart, objectif) - bearing(depart, arrivee);
        }

        /// <summary>
        /// Rend l'angle qui lie les deux points donnés en paramètre
        /// </summary>
        /// <param name="p1">Point 1</param>
        /// <param name="p2">Point 2</param>
        /// <returns>L'angle qui relie les deux points</returns>
        private static double bearing(Position p1, Position p2)
        {
            double y = Math.Sin(p2.Longitude - p1.Longitude) * Math.Cos(p2.Latitude);
            double x = Math.Cos(p1.Latitude) * Math.Sin(p2.Latitude) - Math.Sin(p1.Latitude) * Math.Cos(p2.Latitude) * Math.Cos(p2.Longitude - p1.Longitude);
            return toDeg(Math.Atan2(y, x));
        }

        /// <summary>
        /// Donne la direction de la déviation par rapport à l'objectif (Gauche ou Droite)
        /// </summary>
        /// <returns>Direction de la déviation</returns>
        public DirectionDeviation directionAngleDeviation()
        {
            double a = angleDeviation();
            if (a < 0)
                return DirectionDeviation.GAUCHE;
            else if (a > 0)
                return DirectionDeviation.DROITE;
            else return DirectionDeviation.NONE;
        }

        /// <summary>
        /// Convertit l'angle a en radians vers des degrés
        /// </summary>
        /// <param name="a">L'angle en radians</param>
        /// <returns>L'angle en degrés</returns>
        public static double toDeg(double a)
        {
            return (double)(180 * a / Math.PI);
        }

        /// <summary>
        /// Rend le rapport entre la distance prévue et la distance réelle du tir (en %)
        /// </summary>
        /// <returns>Le rapport en %</returns>
        public double ecartDistance()
        {
			return 100 * (distanceDepartArrivee() / distanceDepartObjectif());
        }

        /// <summary>
        /// Rend la différence entre la distance prévue et réelle
        /// </summary>
        /// <returns>La différence en mètres</returns>
        public double diffDistance()
        {
            return distanceDepartArrivee() - distanceDepartObjectif();
        }

        /// <summary>
        /// Enregistre le tir en cours
        /// </summary>
        /// <param name="dataFolder">Data folder.</param>
        public void enregistrerTir(IFolder dataFolder)
		{ 	// Ouverture du document Clubs
			IFile clubsFile;
			Task<IFile> t2 = dataFolder.CreateFileAsync(Club.FILENAME_CLUBS_XML, CreationCollisionOption.OpenIfExists);
			t2.Wait();
			clubsFile = t2.Result;
			Task<Stream> t3 = clubsFile.OpenAsync(FileAccess.ReadAndWrite);
			t3.Wait();
			using (Stream s2 = t3.Result) {
				XDocument clubsDocument;
				try{
					clubsDocument = XDocument.Load (s2);
				}catch(XmlException){
					throw;
				}

				// Récupération du noeud contenant le club utilisé
				var data = from clubData in clubsDocument.Elements (Club.XMLNODE_CLUBS).Elements (XMLNODE_CLUB)
						where (int)clubData.Attribute (Club.XMLNODE_IDCLUB) == club.type.GetHashCode ()
					select clubData;

				foreach (XElement el in data) {
					double distanceDepartArrivee = this.distanceDepartArrivee ();
					double distanceObjectifArrivee = this.distanceArriveeObjectif ();

					// récupération du nombre de tirs pour le club et incrémentation
					int nbTirs = (int)el.Attribute (Club.XMLNODE_NBTIRS) + 1;

					// récupération de la moyenne des distances réelles et mise à jour
					double averageDistReelle = (double)el.Attribute (Club.XMLNODE_AVERAGEDISTREELLE);
					if (nbTirs == 1)
						averageDistReelle = distanceDepartArrivee;
					else
						averageDistReelle = ((averageDistReelle * (nbTirs - 1)) + distanceDepartArrivee) / nbTirs;


					// récupération de la variance des distances réelles et mise à jour
					double varianceDistReelle = (double)el.Attribute (Club.XMLNODE_VARIANCEDISTREELLE);
					double sommeCarresDistReelle = (double)el.Attribute (Club.XMLNODE_SOMMECARREDISTREELLE);
					if (nbTirs == 1) {
						sommeCarresDistReelle = Math.Pow (averageDistReelle, 2);
						varianceDistReelle = 0;
					} else {
						sommeCarresDistReelle = sommeCarresDistReelle + Math.Pow (distanceDepartArrivee, 2);
						varianceDistReelle = sommeCarresDistReelle / nbTirs - Math.Pow (averageDistReelle, 2);
					}

					// récupération de la moyenne des distances obj/reelle et mise à jour
					double averageDistObjReelle = (double)el.Attribute (Club.XMLNODE_AVERAGEDISTOBJREELLE);
					if (nbTirs == 1)
						averageDistObjReelle = distanceObjectifArrivee;
					else
						averageDistObjReelle = ((averageDistObjReelle * (nbTirs - 1)) + distanceObjectifArrivee) / nbTirs;


					// récupération de la variance des distances réelles et mise à jour
					double varianceDistObjReelle = (double)el.Attribute (Club.XMLNODE_VARIANCEDISTOBJREELLE);
					double sommeCarresDistObjReelle = (double)el.Attribute (Club.XMLNODE_SOMMECARREDISTOBJREELLE);
					if (nbTirs == 1) {
						sommeCarresDistObjReelle = Math.Pow (averageDistObjReelle, 2);
						varianceDistObjReelle = 0;
					} else {
						sommeCarresDistObjReelle = sommeCarresDistObjReelle + Math.Pow (distanceObjectifArrivee, 2);
						varianceDistObjReelle = sommeCarresDistObjReelle / nbTirs - Math.Pow (averageDistObjReelle, 2);
					}

					// récupération de la moyenne des angles et mise à jour
					double angle = angleDeviation ();
					double averageAngle = (double)el.Attribute (Club.XMLNODE_AVERAGEANGLE);
					if (nbTirs == 1)
						averageAngle = angle;
					else
						averageAngle = ((averageAngle * (nbTirs - 1)) + angle) / nbTirs;


					// récupération de la variance des angles et mise à jour
					double varianceAngle = (double)el.Attribute (Club.XMLNODE_VARIANCEANGLE);
					double sommeCarresAngle = (double)el.Attribute (Club.XMLNODE_SOMMECARREANGLE);
					if (nbTirs == 1) {
						sommeCarresAngle = Math.Pow (averageAngle, 2);
						varianceAngle = 0;
					} else {
						sommeCarresAngle = sommeCarresAngle + Math.Pow (angle, 2);
						varianceAngle = sommeCarresAngle / nbTirs - Math.Pow (averageAngle, 2);
					}



					// Mise à jour des attributs
					el.SetAttributeValue (Club.XMLNODE_AVERAGEANGLE, averageAngle);
					el.SetAttributeValue (Club.XMLNODE_SOMMECARREANGLE, sommeCarresAngle);
					el.SetAttributeValue (Club.XMLNODE_VARIANCEANGLE, varianceAngle);
					el.SetAttributeValue (Club.XMLNODE_AVERAGEDISTREELLE, averageDistReelle);
					el.SetAttributeValue (Club.XMLNODE_SOMMECARREDISTREELLE, sommeCarresDistReelle);
					el.SetAttributeValue (Club.XMLNODE_VARIANCEDISTREELLE, varianceDistReelle);
					el.SetAttributeValue (Club.XMLNODE_AVERAGEDISTOBJREELLE, averageDistObjReelle);
					el.SetAttributeValue (Club.XMLNODE_SOMMECARREDISTOBJREELLE, sommeCarresDistObjReelle);
					el.SetAttributeValue (Club.XMLNODE_VARIANCEDISTOBJREELLE, varianceDistObjReelle);
					el.SetAttributeValue (Club.XMLNODE_NBTIRS, nbTirs);

				}

				// Enregistrement dans le flux et libération des ressources
				s2.SetLength (0);
				s2.Position = 0;
				clubsDocument.Save (s2);
			}

            // Ouverture du document Tirs
            IFile tirsFile;
            Task<IFile> t01 = dataFolder.CreateFileAsync(FILENAME_TIRS_XML, CreationCollisionOption.OpenIfExists);
            t01.Wait();
            tirsFile = t01.Result;

            Task<Stream> t1 = tirsFile.OpenAsync(FileAccess.ReadAndWrite);
            XDocument tirs;
            XElement tirsElement;
            t1.Wait();
			using (Stream s1 = t1.Result) {

				if (s1.Length == 0) {
					// Cas où le noeud 'tirs' n'existe pas
					tirsElement = new XElement (XMLNODE_TIRS);
					tirs = new XDocument ();
					tirs.AddFirst (tirsElement);
				} else {
					// cas où le noeud existe
					tirs = XDocument.Load (s1);
					tirsElement = tirs.Element (XMLNODE_TIRS);
				}

				// Création du noeud contenant le tir à ajouter
				XElement tir =
					new XElement (XMLNODE_TIR,
						new XElement (XMLNODE_DEPART,
							new XElement (XMLNODE_LATITUDE, depart.Latitude),
							new XElement (XMLNODE_LONGITUDE, depart.Longitude)
						),
						new XElement (XMLNODE_ARRIVEE,
							new XElement (XMLNODE_LATITUDE, arrivee.Latitude),
							new XElement (XMLNODE_LONGITUDE, arrivee.Longitude)
						),
						new XElement (XMLNODE_OBJECTIF,
							new XElement (XMLNODE_LATITUDE, objectif.Latitude),
							new XElement (XMLNODE_LONGITUDE, objectif.Longitude)
						),
						new XElement (XMLNODE_CLUB, club.type.GetHashCode ()),
						new XElement (XMLNODE_ECARTOBJECTIF, this.distanceArriveeObjectif ()),
						new XElement (XMLNODE_DISTOBJECTIF, this.distanceDepartObjectif ()),
						new XElement (XMLNODE_DISTREELLE, this.distanceDepartArrivee ()),
						new XElement (XMLNODE_ANGLE, this.angleDeviation ()),
						new XElement (XMLNODE_DIRECTION, this.directionAngleDeviation ())
					);

				// Ajout du tir dans le fichier de données
				// en tant qu'enfant du noeud 'tirs'
				// et libération du flux
				tirsElement.Add (tir);
				s1.SetLength (0);
				tirs.Save (s1);
			}

           
        }
    }
}

