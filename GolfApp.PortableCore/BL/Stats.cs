﻿using System;
using GolfApp.Core.BL;
using System.Xml.Linq;
using System.Linq;
using PCLStorage;
using System.IO;
using System.Threading.Tasks;
using System.Globalization;

namespace GolfApp.Core.BL
{
    /// <summary>
    /// Constructeur de l'objet Stats
    /// indiceDistance permet de construire les légendes pour le graphique "Stats Distance"
    /// indiceDeviation permet de construire les légendes pour le graphie "Stats Deviation"
    /// gauche est utilisé pour construire les légendes pour le graphique "Stats Deviation"
    /// </summary>
    public class Stats
    {
		private int indiceDistanceClub;
		private int indiceDistance;
		private int indiceDeviation;
        public float[] donneesTir;
        public int cpt = 0;

		/// <summary>
		/// Initializes a new instance of the <see cref="GolfApp.Core.BL.Stats"/> class.
		/// </summary>
        public Stats()
        {
			indiceDistanceClub = 0;
			indiceDistance = 0;
			indiceDeviation = -90;
			donneesTir = new float[12];
        }

		/// <summary>
		/// Rend un tableau contenant les fréquences de tirs pour des distances à l'objectif allant de 0 à 50 par pas de 5
		/// </summary>
		/// <returns>Le tableau de fréquences</returns>
		/// <param name="dataFolder">Data folder.</param>
		public float[] getDistanceObjectif(IFolder dataFolder, Club.typeClub club)
        {
            IFile tirsFile;
            Task<IFile> t01 = dataFolder.CreateFileAsync(Tir.FILENAME_TIRS_XML, CreationCollisionOption.OpenIfExists);
            t01.Wait();
            tirsFile = t01.Result;
            Task<Stream> t2 = tirsFile.OpenAsync(FileAccess.ReadAndWrite);
            t2.Wait();
			using (Stream s2 = t2.Result) {
				XDocument tirsDocument = XDocument.Load (s2);

				for (int j = 0; j < 120; j += 10) {
					var data = from tirData in tirsDocument.Elements (Tir.XMLNODE_TIRS).Elements (Tir.XMLNODE_TIR)
					                      where (parse (tirData.Element (Tir.XMLNODE_ECARTOBJECTIF).Value) > j)
					                          &&
					                          (parse (tirData.Element (Tir.XMLNODE_ECARTOBJECTIF).Value) < j + 10)
					                          && ((club == Club.typeClub.general) || (int.Parse (tirData.Element (Tir.XMLNODE_CLUB).Value)) == club.GetHashCode ())
					                      select tirData;
					int i = data.Count ();
					this.donneesTir [j / 10] = i;
				}
			}
            return donneesTir;
        }


		public float[] getDistanceClub(IFolder dataFolder, Club.typeClub club)
		{
			IFile tirsFile;
			Task<IFile> t01 = dataFolder.CreateFileAsync(Tir.FILENAME_TIRS_XML, CreationCollisionOption.OpenIfExists);
			t01.Wait();
			tirsFile = t01.Result;
			Task<Stream> t2 = tirsFile.OpenAsync(FileAccess.ReadAndWrite);
			t2.Wait();
			float[] donnees;
			using (Stream s2 = t2.Result) {
				XDocument tirsDocument = XDocument.Load (s2);

				var requete = (from tirData in tirsDocument.Elements (Tir.XMLNODE_TIRS).Elements (Tir.XMLNODE_TIR)
				              where ((club == Club.typeClub.general) || (int.Parse (tirData.Element (Tir.XMLNODE_CLUB).Value)) == club.GetHashCode ())
				              select (float.Parse (tirData.Element (Tir.XMLNODE_DISTREELLE).Value)));


				int min = requete.Any () ? ((int)requete.Min ()) : 0;
				int max = requete.Any () ? ((int)requete.Max ()) : 0;
				/*
			int max = (int)((from tirData in tirsDocument.Elements (Tir.XMLNODE_TIRS).Elements (Tir.XMLNODE_TIR)
				where ((club == Club.typeClub.general) || (int.Parse (tirData.Element (Tir.XMLNODE_CLUB).Value)) == club.GetHashCode ())
				select (float.Parse (tirData.Element (Tir.XMLNODE_DISTREELLE).Value))).Max ());
			*/
				min = (min / 10) * 10;
				max = (max / 10) * 10;
				this.indiceDistanceClub = min;
				donnees = new float[(max - min) / 10 + 1];

				for (int j = min, k = 0; j <= max; j += 10) {
					var data = from tirData in tirsDocument.Elements (Tir.XMLNODE_TIRS).Elements (Tir.XMLNODE_TIR)
					          where (parse (tirData.Element (Tir.XMLNODE_DISTREELLE).Value) > j)
					              &&
					              (parse (tirData.Element (Tir.XMLNODE_DISTREELLE).Value) < j + 10)
					              && ((club == Club.typeClub.general) || (int.Parse (tirData.Element (Tir.XMLNODE_CLUB).Value)) == club.GetHashCode ())
					          select tirData;
					int i = data.Count ();
					donnees [k++] = i;
				}
			}
			return donnees;
		}

		/// <summary>
		/// Rend un tableau contenant les fréquences d'angle de déviation de -90° à +90° par pas de 15°
		/// </summary>
		/// <returns>Le tableau de fréquences</returns>
		/// <param name="dataFolder">Data folder.</param>
		/// <param name="club">Le club pour lequel on veut les statistiques</param>
        public float[] getStatsDeviation(IFolder dataFolder, Club.typeClub club)
        {
            IFile tirsFile;
            Task<IFile> t01 = dataFolder.CreateFileAsync(Tir.FILENAME_TIRS_XML, CreationCollisionOption.OpenIfExists);
            t01.Wait();
            tirsFile = t01.Result;

            Task<Stream> t2 = tirsFile.OpenAsync(FileAccess.ReadAndWrite);
            t2.Wait();
			using (Stream s2 = t2.Result) {
				XDocument tirsDocument = XDocument.Load (s2);
				int k = 0;
				for (int j = -90; j < 90; j += 15, k++) {
					var data = from tirData in tirsDocument.Element (Tir.XMLNODE_TIRS).Elements (Tir.XMLNODE_TIR)
					                      where (parse (tirData.Element (Tir.XMLNODE_ANGLE).Value) > j)
					                          &&
					                          (parse (tirData.Element (Tir.XMLNODE_ANGLE).Value) < j + 15)
					                          && ((club == Club.typeClub.general) || (int.Parse (tirData.Element (Tir.XMLNODE_CLUB).Value)) == club.GetHashCode ())
					                      select tirData;

					int i = data.Count ();
					this.donneesTir [k] = i;
				}
			}

            return donneesTir;
        }

		/// <summary>
		/// Convertit un string en float
		/// </summary>
		/// <param name="s">S.</param>
        private float parse(String s)
        {
            return float.Parse(s, CultureInfo.InvariantCulture  );
        }

		/// <summary>
		/// Rend une légende sous chaque barre du graphique, sous la forme "0-5m" pour représenter l'intervalle
		/// </summary>
		/// <returns>The legend distance.</returns>
        public String getLegendDistance()
        {
            String res;
            res = "" + indiceDistance + "-";
            indiceDistance += 5;
            res += indiceDistance + "m";

            return res;
        }
	

		/// <summary>
		/// Rend une légende sous chaque barre du graphique, sous la forme "0-10m" pour représenter l'intervalle
		/// </summary>
		/// <returns>The legend distance.</returns>
		public String getLegendClubDistance()
		{
			String res;
			res = "" + indiceDistanceClub + "-";
			indiceDistanceClub += 10;
			res += indiceDistanceClub + "m";

			return res;
		}
		/// <summary>
		/// On affiche l'intervalle de distance et la direction (G ou D) pour chaque barre du graphique
		/// </summary>
		/// <returns>The legend deviation.</returns>
        public String getLegendDeviation()
        {
            String res;
			res = "" + indiceDeviation + "-" +  (indiceDeviation+15) + "°";
            indiceDeviation += 15;
            return res;
        }


    }
}

