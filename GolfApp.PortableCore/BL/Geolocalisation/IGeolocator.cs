﻿using GolfApp.Core.BL.Geolocalisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GolfApp.Core.BL.Geolocalisation
{
    public abstract class IGeolocator
    {
		public event EventHandler<PositionErrorEventArgs> PositionError;
		public event EventHandler<PositionEventArgs> PositionChanged;

        public bool IsListening;

        public double DesiredAccuracy;

        public bool SupportsHeading;

        public bool IsGeolocationAvailable;

        public bool IsGeolocationEnabled;

        public abstract Task<Position> GetPositionAsync(CancellationToken cancelToken);

        public abstract Task<Position> GetPositionAsync(CancellationToken cancelToken, bool includeHeading);

        public abstract Task<Position> GetPositionAsync(int timeout);

        public abstract  Task<Position> GetPositionAsync(int timeout, bool includeHeading);

        public abstract Task<Position> GetPositionAsync(int timeout, CancellationToken cancelToken);

        public abstract Task<Position> GetPositionAsync(int timeout, CancellationToken cancelToken, bool includeHeading);

        public abstract void StartListening(int minTime, double minDistance);

        public abstract void StartListening(int minTime, double minDistance, bool includeHeading);

		public abstract void StopListening();

		protected Position lastPosition;

		
	}
}

