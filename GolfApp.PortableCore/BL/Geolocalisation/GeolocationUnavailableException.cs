using Core.BL.Geolocalisation;
using GolfApp.Core.BL.Geolocalisation;
using GolfApp.Core.DAL;
using GolfApp.Core.SAL;
using PCLStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GolfApp.Core
{
	class GeolocationUnavailableException : Exception
	{
		public GeolocationUnavailableException (string s) : base (s)
		{

		}
	}
}
