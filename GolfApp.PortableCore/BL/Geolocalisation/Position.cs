﻿//
//  Copyright 2011-2013, Xamarin Inc.
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
using System;

namespace Core.BL.Geolocalisation
{
    public class Position
    {

        public Position()
        {
        }

        public Position(double latitude, double longitude)
        {
            Timestamp = DateTime.Now;
            Latitude = latitude;
            Longitude = longitude;
            Altitude = 0;
            AltitudeAccuracy = 0;
            Accuracy = 0;
            Heading = 0;
            Speed = 0;
        }

        public Position(Position position)
        {
            if (position == null)
                throw new ArgumentNullException("position");

            Timestamp = position.Timestamp;
            Latitude = position.Latitude;
            Longitude = position.Longitude;
            Altitude = position.Altitude;
            AltitudeAccuracy = position.AltitudeAccuracy;
            Accuracy = position.Accuracy;
            Heading = position.Heading;
            Speed = position.Speed;
        }

        public DateTimeOffset Timestamp
        {
            get;
            set;
        }

        /// <summary>
        /// Rayon de la Terre en km
        /// </summary>
        private const double rayonTerre = 6371;

        /// <summary>
        /// Calcule la distance en mètres entre la localisation courante, et la localisation passée en paramètre
        /// </summary>
        /// <param name="l">Location à tester</param>
        /// <returns>La distance</returns>
        public double distance(Position l)
        {
            // Calculs des distances en radians
            double dLat = toRad(l.Latitude - this.Latitude);
            double dLon = toRad(l.Longitude - this.Longitude);

            double lat1 = toRad(l.Latitude);
            double lat2 = toRad(this.Latitude);

            // a = sin²(dLat/2) + cos(lat1).cos(lat2).sin²(dLon/2)
            double a = (double)(Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2));

            // c = 2.atan2(va, v(1-a))
            double c = (double)(2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a)));

            return rayonTerre * c * 1000;
        }

        /// <summary>
        /// Convertit les degrés en radians
        /// </summary>
        /// <param name="a">L'angle en degrés</param>
        /// <returns>L'angle en radians</returns>
        private static double toRad(double a)
        {
            return (double)(Math.PI * a / 180);
        }

        /// <summary>
        /// Convertit les radians en degrés
        /// </summary>
        /// <param name="a">L'angle en radians</param>
        /// <returns>L'angle en degrés</returns>
        private static double toDeg(double a)
        {
            return (double)(180 * a / Math.PI);
        }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        public double Latitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        public double Longitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the altitude in meters relative to sea level.
        /// </summary>
        public double Altitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the potential position error radius in meters.
        /// </summary>
        public double Accuracy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the potential altitude error range in meters.
        /// </summary>
        /// <remarks>
        /// Not supported on Android, will always read 0.
        /// </remarks>
        public double AltitudeAccuracy
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the heading in degrees relative to true North.
        /// </summary>
        public double Heading
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the speed in meters per second.
        /// </summary>
        public double Speed
        {
            get;
            set;
        }
    }

    public class PositionEventArgs
        : EventArgs
    {
        public PositionEventArgs(Position position)
        {
            if (position == null)
                throw new ArgumentNullException("position");

            Position = position;
        }

        public Position Position
        {
            get;
            private set;
        }
    }

    public class GeolocationException
        : Exception
    {
        public GeolocationException(GeolocationError error)
            : base("A geolocation error occured: " + error)
        {
            if (!Enum.IsDefined(typeof(GeolocationError), error))
                throw new ArgumentException("error is not a valid GelocationError member", "error");

            Error = error;
        }

        public GeolocationException(GeolocationError error, Exception innerException)
            : base("A geolocation error occured: " + error, innerException)
        {
            if (!Enum.IsDefined(typeof(GeolocationError), error))
                throw new ArgumentException("error is not a valid GelocationError member", "error");

            Error = error;
        }

        public GeolocationError Error
        {
            get;
            private set;
        }
    }

    public class PositionErrorEventArgs
        : EventArgs
    {
        public PositionErrorEventArgs(GeolocationError error)
        {
            Error = error;
        }

        public GeolocationError Error
        {
            get;
            private set;
        }
    }

    public enum GeolocationError
    {
        /// <summary>
        /// The provider was unable to retrieve any position data.
        /// </summary>
        PositionUnavailable,

        /// <summary>
        /// The app is not, or no longer, authorized to receive location data.
        /// </summary>
        Unauthorized
    }
}