﻿using System;
using System.Xml.Serialization;

namespace GolfApp.Core.BL
{
    /// <summary>
    /// Représente une couleur avec transparence
    /// </summary>
    public class MyColor
    {
        [XmlAttribute("Red")]
        /// <summary>
        /// Composante rouge de la couleur
        /// Valeurs valables [0,255]
        /// </summary>
        public Byte Red { get; set; }

        [XmlAttribute("Green")]
        /// <summary>
        /// Composante verte de la couleur
        /// Valeurs valables [0,255]
        /// </summary>
        public Byte Green { get; set; }

        [XmlAttribute("Blue")]
        /// <summary>
        /// Composante bleue de la couleur
        /// Valeurs valables [0,255]
        /// </summary>

        public Byte Blue { get; set; }

        [XmlAttribute("Alpha")]
        /// <summary>
        /// Transparence
        /// Valeurs valables [0,255]
        /// </summary>
        public Byte Alpha { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="red"></param>
        /// <param name="green"></param>
        /// <param name="blue"></param>
        /// <param name="alpha"></param>
        public MyColor(Byte red, Byte green, Byte blue, Byte alpha)
        {
            this.Red = red;
            this.Green = green;
            this.Blue = blue;
            this.Alpha = alpha;
        }

        public MyColor()
        {
            this.Red = 0;
            this.Green = 0;
            this.Blue = 0;
            this.Alpha = 0;
        }

        /// <summary>
        /// Convertit la couleur au format HUE
        /// http://en.wikipedia.org/wiki/Hue
        /// L'information de transparence n'est pas supportée donc perdue
        /// </summary>
        /// <returns>Un nombre représentant la couleur</returns>
        public float toHUE()
        {
            return (float)(Math.Atan2(Math.Sqrt(3) * (Green - Blue), 2 * Red - Green - Blue));
        }

        /// <summary>
        /// Convertit la couleur au format ARGB
        /// </summary>
        /// <returns>Un entier 32 bit représentant la couleur</returns>
        public Int32 toARGB()
        {
            Int32 result = (Int32)Blue;
            result += (Int32)(Green) << 8;
            result += (Int32)(Red) << 16;
            result += (Int32)(Alpha) << 24;

            return result;
        }
    }
}
