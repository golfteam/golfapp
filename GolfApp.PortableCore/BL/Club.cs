﻿using PCLStorage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GolfApp.Core.BL
{
    public class Club
    {

        #region Enumérations
        public enum typeClub
        {
            driver,
            bois_3,
            bois_5,
            fer_3,
            fer_4,
            fer_5,
            fer_6,
            fer_7,
            fer_8,
            fer_9,
            pitch,
			sandwedge,
			general
        }
        #endregion

        #region Constantes
        /// <summary>
        /// Permet de connaître la distance par défaut associée à un club
        /// Données tirées de http://fr.wikipedia.org/wiki/Club_(golf) 
        /// </summary>
        private static Dictionary<typeClub, double> defautDistance = new Dictionary<typeClub, double>
		 { 
			{ typeClub.driver, 230 },
			{ typeClub.bois_3, 207.5 },
			{ typeClub.bois_5, 190 },
			{ typeClub.fer_3, 180 },
			{ typeClub.fer_4, 167.5 },
			{ typeClub.fer_5, 155 },
			{ typeClub.fer_6, 145 },
			{ typeClub.fer_7, 135 },
			{ typeClub.fer_8, 125 },
			{ typeClub.fer_9, 112.5 },
			{ typeClub.pitch, 97.5 },
			{ typeClub.sandwedge, 75 }
		};

        public const string XMLNODE_IDCLUB = "IDClub";
		public const string XMLNODE_AVERAGEDISTREELLE = "averageReel";
		public const string XMLNODE_VARIANCEDISTREELLE = "varianceReel";
		public const string XMLNODE_SOMMECARREDISTREELLE = "sommeCarresReel";
		public const string XMLNODE_AVERAGEDISTOBJREELLE = "averageObjReel";
		public const string XMLNODE_VARIANCEDISTOBJREELLE = "varianceObjReel";
		public const string XMLNODE_SOMMECARREDISTOBJREELLE = "sommeCarresObjReel";
        public const string XMLNODE_CLUB = "club";
        public const string XMLNODE_CLUBS = "clubs";
        public const string XMLNODE_NAME = "name";
        public const string XMLNODE_NBTIRS = "nbTirs";
        public const string FILENAME_CLUBS_XML = "clubs.xml";

        public const string XMLNODE_SOMMECARREANGLE = "sommeCarresAngle";
        public const string XMLNODE_AVERAGEANGLE = "averageAngle";
        public const string XMLNODE_VARIANCEANGLE = "varianceAngle";


        #endregion

        #region Attributs et accesseurs
        /// <summary>
        /// Type du club
        /// </summary>
        public typeClub type { get; set; }
        #endregion

        #region Constructeurs
        /// <summary>
        /// Construit un nouveau club avec le type donné
        /// </summary>
        /// <param name="type">Le type à donner au club</param>
        public Club(typeClub type)
        {
            this.type = type;
        }

        #endregion

        #region Méthodes statiques
        /// <summary>
        /// Donne la distance par défaut associée à un club
        /// Rend -1 si le club n'existe pas.
        /// </summary>
        /// <param name="t">Le club à tester</param>
        /// <returns>la distance par défaut en mètres</returns>


        public static double getDefautDistance(typeClub t)
        {
            double d = -1;
            defautDistance.TryGetValue(t, out d);
            return d;
        }

        /// <summary>
        /// Crée le fichier xml initial contenant les données sur les clubs
        /// Attention : remplace si le fichier existe déjà, réinitialisant toutes les données
        /// </summary>
        /// <param name="rootFolder">Emplacement du dossier où enregistrer les données</param>
        /// <returns>Task contenant les actions à exécuter</returns>
		public static void creerFichierXML(IFolder dataFolder)
        {
			Task<IFile> t = dataFolder.CreateFileAsync(FILENAME_CLUBS_XML, CreationCollisionOption.ReplaceExisting);
			t.Wait ();
			IFile file = t.Result;
            Task<Stream> taskS = file.OpenAsync(FileAccess.ReadAndWrite);

			XElement clubs = new XElement(XMLNODE_CLUBS,
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.driver.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Driver"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 230),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.bois_3.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Bois_3"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 207.5),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.bois_5.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Bois_5"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 190),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_3.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_3"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 180),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_4.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_4"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 167.5),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_5.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_5"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 155),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_6.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_6"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 145),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_7.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_7"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 135),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_8.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_8"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 125),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.fer_9.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Fer_9"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 112.5),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.pitch.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Pitch"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 97.5),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0)),
				new XElement(XMLNODE_CLUB,
					new XAttribute(XMLNODE_IDCLUB, typeClub.sandwedge.GetHashCode()),
					new XAttribute(XMLNODE_NAME, "Sandwedge"),
					new XAttribute(XMLNODE_AVERAGEDISTREELLE, 75),
					new XAttribute(XMLNODE_SOMMECARREDISTREELLE, 1),
					new XAttribute(XMLNODE_VARIANCEDISTREELLE, 1),
					new XAttribute(XMLNODE_AVERAGEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_SOMMECARREDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_VARIANCEDISTOBJREELLE, 0),
					new XAttribute(XMLNODE_AVERAGEANGLE, 0),
					new XAttribute(XMLNODE_SOMMECARREANGLE, 0),
					new XAttribute(XMLNODE_VARIANCEANGLE, 1),
					new XAttribute(XMLNODE_NBTIRS, 0))
			);

            taskS.Wait();
			using(Stream s = taskS.Result){
				clubs.Save(s);
			}
        }
        #endregion

        #region Méthodes publiques
        /// <summary>
        /// Rend la distance par defaut associée au club
        /// </summary>
        /// <returns>La distance par défaut en mètres</returns>
        public double getDefautDistance()
        {
            double d = -1;
            defautDistance.TryGetValue(this.type, out d);
            return d;
        }

        /// <summary>
        /// Rend un string du type de club et de sa distance
        /// </summary>
        /// <returns>Un string</returns>
        public String toString()
        {
            return "" + this.type + ":" + this.getDefautDistance();
        }

        #endregion
    }
}
