﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GolfApp.Core.BL
{
	public class Localisation
    {
        #region Constantes
        /// <summary>
        /// Rayon de la Terre en km
        /// </summary>
        private const double rayonTerre = 6371;
        #endregion

        #region Propriétés et accesseurs
        /// <summary>
        /// Latitude de point courant
        /// </summary>
        public double latitude { get; set; }
        /// <summary>
        /// Longitude du point courant
        /// </summary>
        public double longitude { get; set; }

        private double _precision;
        /// <summary>
        /// Précision de la localisation, en mètres
        /// </summary>
        public double precision { 
            get
            {
                return _precision;
            } 
            set 
            { 
                if (value < 0) 
                    _precision = 0; 
                else 
                    _precision = value; 
            } 
        }

        #endregion

        #region Constructeurs
        /// <summary>
        /// Constructeur d'une localisation sans paramètres, attributs initialisés à 0
        /// </summary>
		public Localisation()
        {
            latitude = 0;
            longitude = 0;
            precision = 0;
        }

         /// <summary>
         /// Constructeur d'une localisation avec tous les paramètres
         /// </summary>
         /// <param name="lat">Latitude</param>
         /// <param name="lng">Longitude</param>
         /// <param name="prec">Precision</param>
		public Localisation(double lat, double lng, double prec )
        {
            latitude = lat;
            longitude = lng;
            precision = prec;

        }

        /// <summary>
        /// Constructeur d'une localisation à partir d'une longitude et d'une latitude, la précision est initialisée à 0
        /// </summary>
        /// <param name="lat">Latitude</param>
        /// <param name="lng">Longitude</param>
		public Localisation(double lat, double lng) 
        {
            latitude = lat;
            longitude = lng;
            precision = 0;
        }
        #endregion

        #region Méthodes publiques
        /// <summary>
        /// Calcule la distance en mètres entre la localisation courante, et la localisation passée en paramètre
        /// </summary>
        /// <param name="l">Location à tester</param>
        /// <returns>La distance</returns>
        public double distance(Position l){
            // Calculs des distances en radians
            double dLat = toRad(l.latitude - this.latitude);
            double dLon = toRad(l.longitude - this.longitude);

            double lat1 = toRad(l.latitude);
            double lat2 = toRad(this.latitude);

            // a = sin²(dLat/2) + cos(lat1).cos(lat2).sin²(dLon/2)
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
            Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);

            // c = 2.atan2(va, v(1-a))
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return rayonTerre * c;
        }

        #endregion

        #region Méthodes privées
        /// <summary>
        /// Convertit les degrés en radians
        /// </summary>
        /// <param name="a">L'angle en degrés</param>
        /// <returns>L'angle en radians</returns>
        private static double toRad(double a)
        {
            return (double)(Math.PI * a / 180);
        }

        /// <summary>
        /// Convertit les radians en degrés
        /// </summary>
        /// <param name="a">L'angle en radians</param>
        /// <returns>L'angle en degrés</returns>
        private static double toDeg(double a)
        {
            return (double)(180 * a / Math.PI);
        }
        #endregion
    }

}

