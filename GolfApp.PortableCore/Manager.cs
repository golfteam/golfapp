﻿using Core.BL.Geolocalisation;
using GolfApp.Core.BL;
using GolfApp.Core.DAL;
using GolfApp.Core.SAL;
using PCLStorage;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace GolfApp.Core
{
    public class Manager
    {
        /// Enumération représentant l'étape en cours pour l'utilisateur
        /// (Equivalent à une Activity sur Android)
        public enum Etape
        {
            startGame,
			defineFlag,
            defineObjective,
            choixFer,
            waitingForShot,
            resumeTir,
            finTrou
        }

		/// Représente le système utilisé
		public enum System
		{
			ANDROID,
			IOS
		}

		/// <summary>
		/// Gets or sets the systeme.
		/// </summary>
		/// <value>The systeme.</value>
		public System systeme { get; set; }

        /// <summary>
        /// Correspond à l'étape du cycle en cours
        /// A actualiser dès que nécessaire
        /// </summary>
        /// <value>The etape en cours.</value>
        public Etape etapeEnCours { get; set; }

        /// <summary>
        /// Contient les paramètres de l'application
        /// </summary>
        /// <value>The p.</value>
        public Parametres p { get; private set; }

        /// <summary>
        /// La carte GoogleMaps
        /// </summary>
        /// <value>The map.</value>
        public IPlatformDependantGoogleMap map { get; private set; }

        /// <summary>
        /// The root folder.
        /// </summary>
        private IFolder rootFolder { get; set; }

        /// <summary>
        /// Contient le dossier qui contient les fichiers de données XML
        /// </summary>
        private IFolder dataFolder { get; set; }

        /// <summary>
        /// La position actuelle
        /// </summary>
        public Position position { get; set; }

        /// <summary>
        /// Le marqueur de position
        /// </summary>
        /// <value>The marker position.</value>
        public IPlatformDependantMapsMarker markerPosition { get; private set; }

        /// <summary>
        /// Gets the marker depart.
        /// </summary>
        /// <value>The marker depart.</value>
        public IPlatformDependantMapsMarker markerDepart { get; private set; }

        /// <summary>
        /// Gets the cercle position.
        /// </summary>
        /// <value>The cercle position.</value>
        public IPlatformDependantMapsCircle cerclePosition { get; private set; }

        /// <summary>
        /// Gets the marker objectif.
        /// </summary>
        /// <value>The marker objectif.</value>
        public IPlatformDependantMapsMarker markerObjectif { get; private set; }

        /// <summary>
        /// Gets or sets le par du trou en cours.
        /// </summary>
        /// <value>The par.</value>
        public int par { get; set; }

        /// <summary>
        /// Tir en cours d'ajout
        /// </summary>
        /// <value>Le tir en cours.</value>
        public Tir tirEnCours { get; private set; }

        /// <summary>
        /// Gets the object stats.
        /// </summary>
        /// <value>The object stats.</value>
        public Stats objStats { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="GolfApp.Core.Manager"/> class.
		/// </summary>
		/// <param name="rootLocalAppData">Chemin d'accès vers les fichiers de l'application</param>
		/// <param name="systeme">Système actuellement utilisé (Android/iOS)</param>
		public Manager(string rootLocalAppData, System systeme)
        {

            Task<IFolder> task1 = FileSystem.Current.GetFolderFromPathAsync(rootLocalAppData);
            objStats = new Stats();
			this.systeme = systeme;
            task1.Wait();
            this.rootFolder = task1.Result;

            Task<IFolder> dataFolderTask = rootFolder.CreateFolderAsync("data", CreationCollisionOption.OpenIfExists);
            lireParametres();

            dataFolderTask.Wait();

            dataFolder = dataFolderTask.Result;

            Task<ExistenceCheckResult> clubXMLExiste = dataFolder.CheckExistsAsync(Club.FILENAME_CLUBS_XML);
            clubXMLExiste.Wait();

            // Si le fichier XML qui contient les données des clubs n'existe pas, on le crée
            if (clubXMLExiste.Result == ExistenceCheckResult.NotFound)
            {
               Club.creerFichierXML(dataFolder);
            }
			Task<IFile> task4 = dataFolder.GetFileAsync (Club.FILENAME_CLUBS_XML);
			task4.Wait ();
			Task<Stream> task5 = task4.Result.OpenAsync (FileAccess.Read);
			task5.Wait ();
			using (Stream s = task5.Result) {
				if (s.Length == 0) {
					Club.creerFichierXML (dataFolder);
				}
			}
        }

		/// <summary>
		/// Initialise la carte de résumé du tir
		/// </summary>
		/// <param name="map2">Map2.</param>
		public void carteResumeTir (IPlatformDependantGoogleMap map2)
		{
			map2.init (position, p.MapZoomControlEnabled, p.MapCompassEnabled);
			if (systeme == System.ANDROID) {
				markerObjectif = map2.ajouterMarqueur (markerObjectif.getCoord (), p.DraggableMarqueurObjectif, p.CouleurMarqueurObjectif);
				markerObjectif.setInfoWindowsObjectif ();
				cerclePosition = map2.ajouterCercle (position, position.Accuracy, p.CouleurCerclePosition, p.tailleBordureCerclePosition, p.CouleurBordureCerclePosition);
				markerPosition = map2.ajouterMarqueur (position, p.DraggableMarqueurPosition, p.CouleurMarqueurPosition);
				markerPosition.setInfoWindowsArrivee ();
			}
		}

        /// <summary>
        /// Nettoye le manager des enregistrements liés à une tir précédent, à appeler lorsque le tir est enregistré (ou non) et que l'on démarre un nouveau tir
        /// </summary>
        public void Clear()
        {
            tirEnCours = new Tir();
            markerObjectif = null;
        }

        /// <summary>
        /// A appeler lors de l'ouverture de la fenêtre startGame
        /// </summary>
        /// <param name="map">Map.</param>
        /// <param name="locator">Locator.</param>
        public void startGame(IPlatformDependantGoogleMap map)
        {
            this.map = map;

            map.init(position, p.MapZoomControlEnabled, p.MapCompassEnabled);
			if (systeme == System.ANDROID) {
				markerPosition = map.ajouterMarqueur (position, p.DraggableMarqueurPosition, p.CouleurMarqueurPosition);
				markerPosition.setInfoWindowsPosition ();
				cerclePosition = map.ajouterCercle (position, position.Accuracy, p.CouleurCerclePosition, p.tailleBordureCerclePosition, p.CouleurBordureCerclePosition);
			}
        }

        /// <summary>
        /// A appeler lors d'un appui sur la carte
        /// Ajoute un marqueur d'objectif, ou modifie son emplacement s'il existe
        /// </summary>
        /// <param name="pos">La position de l'appui.</param>
        public void onMapClick(Position pos)
        {
			if (markerObjectif == null) {
				// Pas encore d'objectif => on crée le marqueur
				markerObjectif = map.ajouterMarqueur (pos, p.DraggableMarqueurObjectif, p.CouleurMarqueurObjectif);
				markerObjectif.setInfoWindowsObjectif(position);
			} else {
				if (systeme == System.ANDROID) {
					// Sur Android on change juste les coordonnées du marqueur déjà créé et on change les informations de la bulle d'info
					markerObjectif.setCoord (pos);
					markerObjectif.setInfoWindowsObjectif (position);
				} else if (systeme == System.IOS){
					// Sur iOS, il faut supprimer le marqueur puis en replacer un nouveau
					map.retirerMarqueur (markerObjectif);
					markerObjectif = map.ajouterMarqueur (pos, p.DraggableMarqueurObjectif, p.CouleurMarqueurObjectif);
				}
            }
        }

        /// <summary>
        /// A appeler lors du changement de position
        /// </summary>
        /// <param name="p">Nouvelle Position</param>
        public void OnLocationChanged(Position p)
        {
            position = p;

			if (etapeEnCours == Etape.defineObjective && systeme == System.ANDROID)
            {
                cerclePosition.setCenter(p);
                cerclePosition.setRadius(p.Accuracy);
                markerPosition.setCoord(p);
            }
        }

        /// <summary>
        /// Rend vrai si l'objectif est défini, faux sinon
        /// </summary>
        /// <returns><c>true</c>, if objective defined was ised, <c>false</c> otherwise.</returns>
        public bool isObjectiveDefined()
        {
            return markerObjectif != null;
        }


        public Club.typeClub ferStatChoisi(String fer)
        {

            switch (fer)
            {
                case "Général":
                    return Club.typeClub.general;
                case "Driver":
                    return Club.typeClub.driver;
                case "Bois 3":
                    return Club.typeClub.bois_3;
                case "Bois 5":
                    return Club.typeClub.bois_5;
                case "Fer 3":
                    return Club.typeClub.fer_3;
                case "Fer 4":
                    return Club.typeClub.fer_4;
                case "Fer 5":
                    return Club.typeClub.fer_5;
                case "Fer 6":
                    return Club.typeClub.fer_6;
                case "Fer 7":
                    return Club.typeClub.fer_7;
                case "Fer 8":
                    return Club.typeClub.fer_8;
                case "Fer 9":
                    return Club.typeClub.fer_9;
                case "Pitch":
                    return Club.typeClub.pitch;
                case "Sandwedge":
                    return Club.typeClub.sandwedge;
                default:
                    return Club.typeClub.general;
            }

        }

        /// <summary>
        /// Appeler cette méthode avec le fer choisi en paramètre (en string) pour le tir actuel 
        /// Cette méthode enregistre aussi dans le tir l'objectif et le départ
        /// </summary>
        /// <param name="selectedItem">Fer sélectionné</param>
        public void ferChoisi(String fer)
        {
            tirEnCours = new Tir();
            switch (fer)
            {
                case "Driver":
                    tirEnCours.club = new Club(Club.typeClub.driver);
                    break;
                case "Bois 3":
                    tirEnCours.club = new Club(Club.typeClub.bois_3);
                    break;
                case "Bois 5":
                    tirEnCours.club = new Club(Club.typeClub.bois_5);
                    break;
                case "Fer 3":
                    tirEnCours.club = new Club(Club.typeClub.fer_3);
                    break;
                case "Fer 4":
                    tirEnCours.club = new Club(Club.typeClub.fer_4);
                    break;
                case "Fer 5":
                    tirEnCours.club = new Club(Club.typeClub.fer_5);
                    break;
                case "Fer 6":
                    tirEnCours.club = new Club(Club.typeClub.fer_6);
                    break;
                case "Fer 7":
                    tirEnCours.club = new Club(Club.typeClub.fer_7);
                    break;
                case "Fer 8":
                    tirEnCours.club = new Club(Club.typeClub.fer_8);
                    break;
                case "Fer 9":
                    tirEnCours.club = new Club(Club.typeClub.fer_9);
                    break;
                case "Pitch":
                    tirEnCours.club = new Club(Club.typeClub.pitch);
                    break;
                case "Sandwedge":
                    tirEnCours.club = new Club(Club.typeClub.sandwedge);
                    break;
            }
			if (systeme == System.ANDROID) {
				tirEnCours.depart = markerPosition.getCoord ();
				tirEnCours.objectif = markerObjectif.getCoord ();
			} else if (systeme == System.IOS) {
				tirEnCours.depart = position;
				tirEnCours.objectif = markerObjectif.getCoord ();
			}
        }

		/// <summary>
		/// Lors d'un retournement sous Android, l'objectif disparait
		/// Appeler cette méthode afin de le recréer
		/// </summary>
		public void recreerObjectif ()
		{
			if (isObjectiveDefined()) {
				markerObjectif = map.ajouterMarqueur (markerObjectif.getCoord(), p.DraggableMarqueurObjectif, p.CouleurMarqueurObjectif);
				markerObjectif.setInfoWindowsObjectif(position);
			}
		}

        /// <summary>
        /// A appeler lorsque le point d'arrivée de la balle est défini
        /// </summary>
        /// <param name="p">P.</param>
        public void ajouterArrivee()
        {
            tirEnCours.arrivee = this.position;
        }

        /// <summary>
        /// Enregistre le tir en cours dans la base
        /// </summary>
        public void enregistrerTir()
        {
			try{
            	tirEnCours.enregistrerTir(dataFolder);
			}catch(XmlException){
				// recréation du fichier clubs.xml
				 Club.creerFichierXML (dataFolder);
				// et on retente !
				tirEnCours.enregistrerTir(dataFolder);
			}
        }

		/// <summary>
		/// Récupère les statistiques de déviation correspondant à un club
		/// </summary>
		/// <returns>Un tableau contenant les fréquences d'angle de déviation de -90° à +90° par pas de 15°</returns>
		/// <param name="c">Le club demandé</param>
		public float[] recupererDeviation(Club.typeClub c)
        {
            return objStats.getStatsDeviation(dataFolder, c);
        }

		public float[] recupererDistanceObjectif(Club.typeClub c)
		{
			return objStats.getDistanceObjectif(dataFolder, c);
		}

		public float[] recupererDistanceClub(Club.typeClub c)
		{
			return objStats.getDistanceClub(dataFolder, c);
		}

        /// <summary>
        /// Ouvre le fichier de paramètres avec les droits demandés
        /// à l'adresse suivante : %rootFolder%/params/golf.xml
        /// </summary>
        /// <returns>Un stream pour lire/écrire le fichier</returns>
        /// <param name="f">Les droits d'accès au fichier</param>
        private Stream openParamsFileAsync(FileAccess f)
        {
            Task<IFolder> t1 = rootFolder.CreateFolderAsync("params",
                CreationCollisionOption.OpenIfExists);
            t1.Wait();
            IFolder folder = t1.Result;

            Task<IFile> t2;

            if (f == FileAccess.Read)
                t2 = folder.CreateFileAsync("golf.xml", CreationCollisionOption.OpenIfExists);
            else
                t2 = folder.CreateFileAsync("golf.xml", CreationCollisionOption.ReplaceExisting);

            IFile file = t2.Result;
            Task<Stream> t3 = file.OpenAsync(f);
            t3.Wait();
            return t3.Result;
        }

        /// <summary>
        /// Lit le fichier sérialisé des paramètres et place le nouvel objet en propriété
        /// Si le fichier est inexistant, ou inconsistant, il est recréé avec les paramètres par défaut
        /// </summary>
        public void lireParametres()
        {
            Boolean fichierExistant = true;
            Stream s = openParamsFileAsync(FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(typeof(Parametres));
            try
            {
                p = xs.Deserialize(s) as Parametres;
                fichierExistant = true;
            }
            catch (Exception)
            {
                p = new Parametres();
                fichierExistant = false;
            }
            finally
            {
                s.Dispose();
                if (!fichierExistant)
                {
                    ecrireParametres();
                }
            }
        }

        /// <summary>
        /// Enregistre le fichier sérialisé à partir des paramètres actuels
        /// </summary>
        public void ecrireParametres()
        {
            Stream s = openParamsFileAsync(FileAccess.ReadAndWrite);
            XmlSerializer xs = new XmlSerializer(typeof(Parametres));

            xs.Serialize(s, p);
            s.Dispose();
        }

		public void initStats()
		{
			this.objStats = new Stats ();
		}
    }
}
