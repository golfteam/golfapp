﻿using Core.BL.Geolocalisation;
using System;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;

namespace GolfApp.Core.SAL
{
    public class Weather
    {
        #region Définition des enums
        public enum uniteTemperature
        {
            CELSIUS,
            KELVIN,
            FAHRENHEIT
        }

        #endregion

        #region Définition des constantes

        private const string ADRESSE_SERVICE = "http://api.openweathermap.org/data/2.5/weather?";
        private const string MODE_XML = "mode=xml";
        private const string API_KEY_OPENWEATHERMAP = "APPID=b6dd295b522b9c84112b723fec249c22";
        private const string LAT = "lat=";
        private const string LNG = "lon=";
        private const string SEPARATEUR_PARAMETRES = "&";
        private const string XML_TEMPERATURE = "temperature";
        private const string XML_PRESSURE = "pressure";
        private const string XML_WIND = "wind";
        private const string XML_SPEED = "speed";
        private const string XML_DIRECTION = "direction";

        #endregion

        #region Propriétés et accesseurs
        /// <summary>
        /// Contient le fichier xml retourné par OpenWeatherMap
        /// </summary>
        private string xmlMeteo { get; set; }


        private Position _localisation;
        /// <summary>
        /// L'emplacement de la météo courante
        /// </summary>
        public Position localisation
        {
            get
            {
                return _localisation;
            }
        }

        private float _temperature;
        /// <summary>
        /// La température en Kelvins
        /// </summary>
        public float temperature
        {
            get
            {
                return _temperature;
            }
            private set
            {
                _temperature = value;
            }
        }

        private float _windSpeed;
        /// <summary>
        /// Vitesse du vent en 
        /// </summary>
        public float windSpeed
        {
            get
            {
                return _windSpeed;
            }
            private set
            {
                _windSpeed = value;
            }
        }


        private float _windDirection;
        /// <summary>
        /// Direction du vent en degrés
        /// </summary>
        public float windDirection
        {
            get
            {
                return _windDirection;
            }
            private set
            {
                _windDirection = value;
            }
        }

        private float _pressure;
        /// <summary>
        /// La pression météorologique à l'emplacement ciblé
        /// </summary>
        public float pressure
        {
            get
            {
                return _pressure;
            }
            private set
            {
                _pressure = value;
            }
        }

        /// <summary>
        /// Est mis à true lorsque l'initialisation a été faite
        /// </summary>
        private Boolean _init = false;

        #endregion

        #region Constructeurs
        /// <summary>
        /// Constructeur de la classe Weather
        /// Recherche les informations météo sur la localisation fournie en paramètre en utilisant OpenWeatherMap
        /// Puis tente de placer ces informations dans les propriétés
        /// </summary>
        /// <param name="l">L'emplacement de la météo à rechercher</param>
        public Weather(Position l)
        {
            _localisation = l;
        }
        #endregion

        #region Méthodes publiques
        /// <summary>
        /// Recherche les informations à partir de la locatisation en propriété
        /// </summary>
        public async Task init()
        {
            await recupererXml();
            parseXml();
            _init = true;
        }

        /// <summary>
        /// Teste l'état de l'initialisation de l'objet
        /// </summary>
        /// <returns>Vrai si l'objet a été correctement initialisé</returns>
        public Boolean isInit()
        {
            return _init;
        }
        /// <summary>
        /// Rend la température dans l'unité fournie en paramètre
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public double getTemperature(uniteTemperature u)
        {
            if (!isInit())
                throw new WeatherNotInitializedException();
            double result = 0;
            switch (u)
            {
                case uniteTemperature.CELSIUS:
                    result = temperature - 273.15;
                    break;
                case uniteTemperature.FAHRENHEIT:
                    result = temperature * 9 / 5 - 459.67;
                    break;
                case uniteTemperature.KELVIN:
                    result = temperature;
                    break;
                default:
                    throw new ArgumentException();
            }
            return result;
        }

        #endregion

        #region Méthodes privées
        /// <summary>
        /// Tente de parser le xmlMeteo pour remplir les propriétés
        /// </summary>
        /// <returns></returns>
        private void parseXml()
        {
            using (XmlReader reader = XmlReader.Create(new StringReader(xmlMeteo)))
            {
                reader.ReadToFollowing(XML_TEMPERATURE);
                reader.MoveToFirstAttribute();
                temperature = float.Parse(reader.Value, CultureInfo.InvariantCulture);

                reader.ReadToFollowing(XML_PRESSURE);
                reader.MoveToFirstAttribute();
                pressure = float.Parse(reader.Value, CultureInfo.InvariantCulture);

                reader.ReadToFollowing(XML_SPEED);
                reader.MoveToFirstAttribute();
                windSpeed = float.Parse(reader.Value, CultureInfo.InvariantCulture);

                reader.ReadToFollowing(XML_DIRECTION);
                reader.MoveToFirstAttribute();
                windDirection = float.Parse(reader.Value, CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Tente de récupérer les informations à partir de OpenWeatherMap et place le résultat dans xmlMeteo
        /// </summary>
        private async Task recupererXml()
        {
            var client = new HttpClient();
            xmlMeteo = await client.GetStringAsync(getUri());
        }

        private Uri getUri()
        {
            return new Uri(ADRESSE_SERVICE +
                LAT + localisation.Latitude.ToString(CultureInfo.InvariantCulture
                            ) + SEPARATEUR_PARAMETRES +
                LNG + localisation.Longitude.ToString(CultureInfo.InvariantCulture
                            ) + SEPARATEUR_PARAMETRES +
                            MODE_XML);
        }
        #endregion

    }
}
