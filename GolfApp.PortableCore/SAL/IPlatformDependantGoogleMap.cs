﻿using Core.BL.Geolocalisation;
using GolfApp.Core.BL;
using System;

namespace GolfApp.Core.SAL
{
    /// <summary>
    /// Interface représentant un élément de carte
    /// </summary>
    public interface IPlatformDependantGoogleMap
    {
        /// <summary>
        /// Initialise la carte avec les paramètres fournis et une vue aérienne
        /// </summary>
        /// <param name="defautPosition">Defaut position.</param>
        /// <param name="ZoomControlEnabled">If set to <c>true</c> zoom control enabled.</param>
        /// <param name="CompassEnabled">If set to <c>true</c> compass enabled.</param>
        void init(Position defautPosition, Boolean ZoomControlEnabled, Boolean CompassEnabled);

        /// <summary>
        /// Ajoute un cercle personnalisé à la carte
        /// </summary>
        /// <param name="centre">Emplacement du centre du cercle</param>
        /// <param name="rayon">Rayon du cercle en mètres</param>
        /// <param name="couleurDisque">Couleur de remplissage du disque</param>
        /// <param name="tailleBordure">Taille de la bordure du cercle en pixels</param>
        /// <param name="couleurBordure">Couleur de la bordure du cercle</param>
        /// <returns>Vrai si l'opération s'est bien déroulée</returns>
        IPlatformDependantMapsCircle ajouterCercle(Position centre, double rayon, MyColor couleurDisque, int tailleBordure, MyColor couleurBordure);

        /// <summary>
        /// Retire un marqueur sur la carte
        /// </summary>
        /// <param name="marker">Le marqueur à retirer</param>
        /// <returns>Vrai si l'opération s'est bien déroulée</returns>
        void retirerCercle(GolfApp.Core.SAL.IPlatformDependantMapsCircle cercle);

        /// <summary>
        /// Ajoute un marqueur sur la carte
        /// </summary>
        /// <param name="l">Emplacement du marqueur</param>
        /// <param name="draggable">Possibilité de modifier son emplacement</param>
        /// <param name="couleur">Couleur du marqueur</param>
        /// <returns>Vrai si l'opération s'est bien déroulée</returns>
        IPlatformDependantMapsMarker ajouterMarqueur(Position l, Boolean draggable, MyColor couleur);

        /// <summary>
        /// Retire un marqueur sur la carte
        /// </summary>
        /// <param name="marker">Le marqueur à retirer</param>
        /// <returns>Vrai si l'opération s'est bien déroulée</returns>
        void retirerMarqueur(GolfApp.Core.SAL.IPlatformDependantMapsMarker marker);

    }
}
