﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GolfApp.Core.SAL
{
    class WeatherNotInitializedException : Exception
    {
        public WeatherNotInitializedException() : base("Tentative d'accéder à une propriété de la classe Weather quand celle-ci n'est pas initialisée. Essayez d'appeler init().")
        { }
    }
}
