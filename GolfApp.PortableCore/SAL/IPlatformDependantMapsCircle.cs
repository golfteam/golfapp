﻿using Core.BL.Geolocalisation;

namespace GolfApp.Core.SAL
{
    public interface IPlatformDependantMapsCircle
    {
        /// <summary>
        /// Place le centre du cercle à l'emplacement voulu
        /// </summary>
        /// <param name="l">L'emplacement où placer le centre</param>
        void setCenter(Position l);

        /// <summary>
        /// Définit le rayon du cercle
        /// </summary>
        /// <param name="r">Rayon du cercle</param>
        void setRadius(double r);

        /// <summary>
        /// Rend l'emplacement du centre du cercle
        /// </summary>
        /// <returns>Le centre du cercle</returns>
        Position getCenter();

        /// <summary>
        /// Rend le rayon du cercle
        /// </summary>
        /// <returns>Le rayon du cercle</returns>
        double getRadius();
    }
}

