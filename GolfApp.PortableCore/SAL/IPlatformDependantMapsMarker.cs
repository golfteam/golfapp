﻿using Core.BL.Geolocalisation;
using System;

namespace GolfApp.Core.SAL
{
    public interface IPlatformDependantMapsMarker
    {
        /// <summary>
        /// Rend les coordonnées du marqueur
        /// </summary>
        /// <returns>Les coordonnées</returns>
        Position getCoord();

        /// <summary>
        /// Permet de placer le marqueur à l'emplacement voulu
        /// </summary>
        /// <param name="l">Le nouvel emplacement du marqueur</param>
        void setCoord(Position l);

        /// <summary>
        /// Permet de savoir si le marqueur peut être déplacé
        /// </summary>
        /// <returns></returns>
        Boolean isDraggable();

        /// <summary>
        /// Permet de rendre le marqueur déplaçable ou non
        /// </summary>
        /// <param name="draggable">Vrai si le marqueur peut se déplacer</param>
        void setDraggable(Boolean draggable);

		/// <summary>
		/// Crée ou modifie la fenêtre d'info qui affiche la distance
		/// </summary>
		/// <param name="p">La position de départ</param>
		void setInfoWindowsObjectif (Position p);

		/// <summary>
		/// Crée ou modifie la fenêtre indiquant l'objectif (résumé tir)
		/// </summary>
		void setInfoWindowsObjectif ();

		/// <summary>
		/// Crée la bulle d'info qui indique que le marqueur est la position
		/// </summary>
		void setInfoWindowsPosition();

		/// <summary>
		/// Crée la bulle d'info qui indique que le marqueur est l'arrivée de la balle
		/// </summary>
		void setInfoWindowsArrivee ();
    }
}

