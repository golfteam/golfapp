﻿using Core.BL.Geolocalisation;
using GolfApp.Core.SAL;
using MonoTouch.CoreLocation;
using System;

namespace GolfApp.iOS2
{
    public class MapsMarkerImpl : IPlatformDependantMapsMarker
    {
        public BasicAnnotation marker { get; private set; }

        public MapsMarkerImpl(Position pos)
        {
            CLLocationCoordinate2D location = new CLLocationCoordinate2D(pos.Latitude, pos.Longitude);
            this.marker = new BasicAnnotation(location);
        }

        /// <summary>
        /// Rend les coordonnées du marqueur
        /// </summary>
        /// <returns>Les coordonnées</returns>
        public Position getCoord()
        {
            return new Position(this.marker.Coordinate.Latitude, this.marker.Coordinate.Longitude);
        }

        /// <summary>
        /// Permet de placer le marqueur à l'emplacement voulu
        /// </summary>
        /// <param name="l">Le nouvel emplacement du marqueur</param>
        public void setCoord(Position l)
        {
            // Impossible de modifier la position d'un marqueur via ses coordonnées sur IOS
        }

        /// <summary>
        /// Permet de savoir si le marqueur peut être déplacé
        /// </summary>
        /// <returns></returns>
        public Boolean isDraggable()
        {
            return true;
        }

        /// <summary>
        /// Permet de rendre le marqueur déplaçable ou non
        /// </summary>
        /// <param name="draggable">Vrai si le marqueur peut se déplacer</param>
        public void setDraggable(Boolean draggable)
        {
        }


        public void setInfoWindowsObjectif(Position p)
        {

        }

        public void setInfoWindowsPosition()
        {

        }
		public void setInfoWindowsArrivee()
		{

		}
		public void setInfoWindowsObjectif ()
		{

		}
    }
}

