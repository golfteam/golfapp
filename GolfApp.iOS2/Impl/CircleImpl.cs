﻿using Core.BL.Geolocalisation;
using GolfApp.Core.SAL;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
namespace GolfApp.iOS2
{
    public class CircleImpl : IPlatformDependantMapsCircle
    {
        public MKCircle cercle;
        public MKCircleRenderer cerclerenderer;

        public CircleImpl(Position p, double rayon)
        {
            CLLocationCoordinate2D pos = new CLLocationCoordinate2D(p.Latitude, p.Longitude);
            cercle = MKCircle.Circle(pos, rayon);
        }

        /// <summary>
        /// Place le centre du cercle à l'emplacement voulu
        /// </summary>
        /// <param name="l">L'emplacement où placer le centre</param>
        public void setCenter(Position l)
        {
            // Read only attribute
        }

        /// <summary>
        /// Définit le rayon du cercle
        /// </summary>
        /// <param name="r">Rayon du cercle</param>
        public void setRadius(double r)
        {
            // Read only attribute
        }

        /// <summary>
        /// Rend l'emplacement du centre du cercle
        /// </summary>
        /// <returns>Le centre du cercle</returns>
        public Position getCenter()
        {
            CLLocationCoordinate2D pos = cercle.Coordinate;
            Position res = new Position(pos.Latitude, pos.Longitude);
            return res;
        }

        /// <summary>
        /// Rend le rayon du cercle
        /// </summary>
        /// <returns>Le rayon du cercle</returns>
        public double getRadius()
        {
            return cercle.Radius;
        }
    }
}

