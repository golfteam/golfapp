﻿using Core.BL.Geolocalisation;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using System;
using System.Drawing;

namespace GolfApp.iOS2.Impl
{
    public class GoogleMapImpl : IPlatformDependantGoogleMap
    {
        public MKMapView map { get; private set; }
        protected string annotationIdentifier = "BasicAnnotation";
        public MapDelegate mapDelegate { get; private set; }
        public CircleImpl circleOverlay;

        public GoogleMapImpl(int x, int y, float width, float height)
        {
            map = new MKMapView(new RectangleF(x, y, width, height));
        }

        public void init(Position defautPosition, Boolean ZoomControlEnabled, Boolean CompassEnabled)
        {
            // Définitions des attributs de la map
            // Vue satellite, affiche la posi
            map.MapType = MKMapType.Satellite;
			map.ShowsUserLocation = true;
            map.ZoomEnabled = ZoomControlEnabled;
            map.ScrollEnabled = CompassEnabled;
            mapDelegate = new MapDelegate();
            map.Delegate = mapDelegate;
        }

        public GolfApp.Core.SAL.IPlatformDependantMapsCircle ajouterCercle(Position centre, double rayon, MyColor couleurDisque, int tailleBordure, MyColor couleurBordure)
        {
            circleOverlay = new CircleImpl(centre, rayon);
            map.OverlayRenderer = (m, o) =>
            {
                if (circleOverlay.cerclerenderer == null)
                {
                    circleOverlay.cerclerenderer = new MKCircleRenderer(o as MKCircle);
                    // Comme seules les couleurs Green, Purple et Red sont disponibles pour les marqueurs iOS,
                    // on effectue un test sur la plus grande des 3 composantes red,blue,green et on choisit
                    // la couleur du marqueur en fonction du résultat.
                    if (Math.Max(couleurDisque.Blue, couleurDisque.Green) == couleurDisque.Blue)
                    {
                        if (Math.Max(couleurDisque.Blue, couleurDisque.Red) == couleurDisque.Blue)
                        {
                            circleOverlay.cerclerenderer.FillColor = UIColor.Blue;
                        }
                        else
                        {
                            circleOverlay.cerclerenderer.FillColor = UIColor.Red;
                        }
                    }
                    else if (Math.Max(couleurDisque.Green, couleurDisque.Red) == couleurDisque.Green)
                    {
                        circleOverlay.cerclerenderer.FillColor = UIColor.Green;
                    }
                    else
                    {
                        circleOverlay.cerclerenderer.FillColor = UIColor.Red;
                    }
                    circleOverlay.cerclerenderer.Alpha = 0.3f;
                }
                return circleOverlay.cerclerenderer;
            };
            map.AddOverlay(circleOverlay.cercle);
            return circleOverlay;
        }

        public void retirerCercle(GolfApp.Core.SAL.IPlatformDependantMapsCircle cercle)
        {
            map.RemoveOverlay((cercle as CircleImpl).cercle);
        }

        public GolfApp.Core.SAL.IPlatformDependantMapsMarker ajouterMarqueur(Position l, Boolean draggable, MyColor couleur)
        {
            // Comme seules les couleurs Green, Purple et Red sont disponibles pour les marqueurs iOS,
            // on effectue un test sur la plus grande des 3 composantes red,blue,green et on choisit
            // la couleur du marqueur en fonction du résultat.
            if (Math.Max(couleur.Blue, couleur.Green) == couleur.Blue)
            {
                if (Math.Max(couleur.Blue, couleur.Red) == couleur.Blue)
                {
                    MapDelegate.ColorMarker = 1;
                }
                else
                {
                    MapDelegate.ColorMarker = 2;
                }
            }
            else if (Math.Max(couleur.Green, couleur.Red) == couleur.Green)
            {
                MapDelegate.ColorMarker = 0;
            }
            else
            {
                MapDelegate.ColorMarker = 2;
            }
            // Le changement de la couleur du marqueur s'effectue dans la classe MapDelegate. Il existe peut-être un moyen
            // plus efficace de changer la couleur dans cette classe depuis cette méthode.

            // On ajoute l'annotation à la map.
            MapsMarkerImpl markerImpl = new MapsMarkerImpl(l);
            map.AddAnnotation(markerImpl.marker);
            return markerImpl;
        }


        public void retirerMarqueur(GolfApp.Core.SAL.IPlatformDependantMapsMarker marker)
        {
            map.RemoveAnnotation((marker as MapsMarkerImpl).marker);
        }

    }

}

