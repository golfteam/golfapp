using System;

namespace GolfApp.iOS2
{
	public class PickerChangedEventArgs : EventArgs
	{
		public string SelectedValue {get; set;}
	}
}

