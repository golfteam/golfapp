﻿using System;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;

namespace GolfApp.iOS2
	{
	public class BasicAnnotation : MKAnnotation {

			public override CLLocationCoordinate2D Coordinate {get;set;}
			string title, subtitle;
			public override string Title { get{ return title; }}
			public override string Subtitle { get{ return subtitle; }}

		public BasicAnnotation (CLLocationCoordinate2D coordinate) {
				this.Coordinate = coordinate;
			}

		}
	}
