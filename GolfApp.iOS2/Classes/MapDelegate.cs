﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using MonoTouch.CoreLocation;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;

namespace GolfApp.iOS2
{
	public class MapDelegate : MKMapViewDelegate {
		protected string annotationIdentifier = "BasicAnnotation";
		public static int ColorMarker;
		public override MKAnnotationView GetViewForAnnotation (MKMapView map, NSObject annotation)
		{
			// try and dequeue the annotation view
			MKAnnotationView annotationView = map.DequeueReusableAnnotation(annotationIdentifier);   
			// if we couldn't dequeue one, create a new one
			if (annotationView == null)
				annotationView = new MKPinAnnotationView(annotation, annotationIdentifier);

			else // if we did dequeue one for reuse, assign the annotation to it
				annotationView.Annotation = annotation;

			// Animation lors du positionnement du marqueur
			(annotationView as MKPinAnnotationView).AnimatesDrop = true;

			// Sélectionne la couleur du marqueur en fonction de l'attribut static ColorMarker (mis à jour dans
			// GoogleMapImpl.ajouteMarqueur
			switch (ColorMarker) {
			case 0:
				(annotationView as MKPinAnnotationView).PinColor = MKPinAnnotationColor.Green;
				break;
			case 1:
				(annotationView as MKPinAnnotationView).PinColor = MKPinAnnotationColor.Purple;
				break;
			case 2:
				(annotationView as MKPinAnnotationView).PinColor = MKPinAnnotationColor.Red;
				break;
			default :
				break;
			}


			return annotationView;
		}  
			
		// as an optimization, you should override this method to add or remove annotations as the
		// map zooms in or out.
		public override void RegionChanged (MKMapView mapView, bool animated) {}
	}
}

