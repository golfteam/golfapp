﻿using GolfApp.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;

namespace GolfApp.iOS2
{
    // This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        UIWindow window;
        MainViewController mainViewController;
        Manager manager;
        //
        // This method is invoked when the application has loaded and is ready to run. In this
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            // On crée notre manager, qui controlera toute l'application
            // On lui spécifie l'emplacement où créer les fichiers de données
            manager = new Manager(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Manager.System.IOS);
            // On crée notre window, qui aura comme taille la taille de l'écran entier
            window = new UIWindow(UIScreen.MainScreen.Bounds);

            // On déclare notre NavigationController, c'est lui qui gère toutes les vues
            // de l'application (fonctionne comme une pile dans laquelle on ajoute ou retire
            // des ViewController ( = Activity sur Android )
            var rootNavigationController = new UINavigationController();

            // On définit notre Controller pour cette première vue de l'application
            // Nouvelle instance de MainViewController
            mainViewController = new MainViewController();
            // On lui attribut le manager
            mainViewController.manager = manager;

            // On ajoute notre mainViewController dans la pile NavigationController.
            // Il s'agit de la vue au sommet de la pile des vues
            rootNavigationController.PushViewController(mainViewController, false);

            // On attribue à window son NavigationController
            window.RootViewController = rootNavigationController;
            // On affiche window
            window.MakeKeyAndVisible();

            return true;
        }
    }
}

