﻿using MonoTouch.UIKit;
using System.Drawing;
using GolfApp.Core;

namespace GolfApp.iOS2
{
    public partial class PreShotViewController : UIViewController
    {
        #region Déclarations
		public Manager manager;
        UIView main;
        UIView flecheView;
        UIButton jySuis;
        TargetViewController targetViewController;
        ShotViewController shotViewController;
        #endregion

        #region Méthodes publiques
        // Constructeur de notre TargetViewController 
        public PreShotViewController()
            : base("PreShotViewController", null)
        {
            this.Title = "Avancez à votre balle";
        }

        // Set un TargetViewController à l'attribut de notre PreShotViewController
        // Utile pour récupérer les données de la précédente mapView
        public void setTargetView(TargetViewController targetview)
        {
            this.targetViewController = targetview;
        }

        // Get le TargetViewController de notre PreShotViewController
        public TargetViewController getTargetView()
        {
            return this.targetViewController;
        }
        #endregion

        // Chargement de la vue
        public override void LoadView()
        {
			manager = this.targetViewController.manager;
            // main : vue principale, taille full screen
            main = new UIView(UIScreen.MainScreen.Bounds);
            // flecheview : vue contenant fleche.png
            flecheView = new UIView(new RectangleF(30, 60, main.Frame.Width, main.Frame.Height / 2));
            // Définition de la textview
            var textView = new UILabel(new RectangleF(0, main.Frame.Height / 2 + 60, main.Frame.Width, main.Frame.Height / 2 - 100));
            textView.Text = "Une fois à votre balle, passez à l'étape suivante. Vous pourrez choisir d'enregistrer ou non ce coup dans les données.";
            textView.Font = UIFont.FromName("Helvetica-Bold", 18f);
            textView.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);
            textView.LineBreakMode = UILineBreakMode.WordWrap;
            textView.TextAlignment = UITextAlignment.Center;
            textView.Lines = 0;
            // Définition du bouton jySuis
            jySuis = new UIButton(UIButtonType.RoundedRect);
            jySuis.SetTitle("J'y suis", UIControlState.Normal);
            jySuis.Frame = new RectangleF(main.Frame.Width - 90, main.Frame.Height - 40, 90, 40);
            jySuis.BackgroundColor = UIColor.White;
            // On ajoute les subview à la vue main
            main.AddSubview(textView);
            main.AddSubview(jySuis);
            main.AddSubview(flecheView);
            View = main;
            // On définit le fond d'écran (dans Resources/Images) et l'image fleche
            View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("Images/FrontImage.jpg"));
            flecheView.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("Images/fleche.png"));
			// Etape du manager
			manager.etapeEnCours = Manager.Etape.waitingForShot;

        }

        public override void ViewDidLoad()
        {
            jySuis.TouchDown += (sender, e) =>
            {
				// On incrémente le nombre de coup du joueur
				TargetViewController.nbCoup++;
                if (shotViewController == null)
                { shotViewController = new ShotViewController(); }
                //---- push our mapview screen onto the navigation
                //controller and pass a true so it navigates
                shotViewController.setPreShotView(this);
                this.NavigationController.PushViewController(shotViewController, true);
            };
        }
    }
}


