﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using BarChart;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;
using GolfApp.iOS2;

namespace GolfApp.iOS2
{
	public partial class StatsDistanceViewController : UIViewController
	{
		UIView barview;
		Stats obj;

		// Constructeur de notre StatsDistanceViewController 
		// Surtout utile pour ajouter un titre à la vue
		public StatsDistanceViewController () : base ("StatsDistanceViewController", null) {
			this.Title = "Distance à l'objectif";
		}

		public override void LoadView () {
			// main : vue principale, taille full screen
			UIView main = new UIView(UIScreen.MainScreen.Bounds);
			barview = new UIView(new RectangleF(0,60,UIScreen.MainScreen.Bounds.Width,UIScreen.MainScreen.Bounds.Height-60));
			main.AddSubview (barview);
			View = main;

		}

		public override void ViewDidLoad () {
		
			base.ViewDidLoad (); 

			obj = new Stats ();

			var data = new[] { 1f, 2f, 4f, 8f, 16f, 32f };
			var chart = new BarChartView {
				Frame = barview.Frame,
				ItemsSource = Array.ConvertAll (data, v => new BarModel { Value = v, Legend = obj.getLegendDistance()})
			};

			// On enlève l'échelle des axes par défaut
			chart.AutoLevelsEnabled = false;
			// Permet si true de cacher la légende en ordonnées (laisse un espace noir...)
			chart.LevelsHidden = false;

					
			View.AddSubview (chart);
		}



	}
}

