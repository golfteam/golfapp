﻿using Core.BL.Geolocalisation;
using GolfApp.Core.SAL;
using GolfApp.Core.BL;
using GolfApp.Core;
using GolfApp.iOS2.Impl;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using System.Drawing;

namespace GolfApp.iOS2
{
    public partial class ShotViewController : UIViewController
    {
        #region Déclarations
		Manager manager;
        UIView main;
        UIView buttons;
        GoogleMapImpl gmap;
        MKMapView map;
        GolfApp.Core.SAL.IPlatformDependantMapsMarker shotmarker;
        TargetViewController targetViewController;
		GreenViewController greenViewController;
        UIButton enregistrer;
        UIButton nePasEnregistrer;
		UIButton surLeGreen;
        PreShotViewController preShotViewController;
        #endregion

        #region Méthodes publiques
        // Constructeur de notre TargetViewController 
        public ShotViewController()
            : base("ShotViewController", null)
        {
            this.Title = "Résumé du coup";
        }

        // Set un TargetViewController à l'attribut de notre ShotViewController
        // Utile pour récupérer les données des précédentes mapView
        public void setPreShotView(PreShotViewController preshotview)
        {
            this.preShotViewController = preshotview;
        }
        #endregion

        // Chargement de la vue
        public override void LoadView()
        {
            #region Déclarations
			manager = this.preShotViewController.manager;
            // main : vue principale, taille full screen
            main = new UIView(UIScreen.MainScreen.Bounds);
            // Initialisation de la google map
            gmap = this.preShotViewController.getTargetView().gmap;
            map = gmap.map;
            map.RemoveGestureRecognizer(this.preShotViewController.getTargetView().tap);
			map.RemoveGestureRecognizer(this.preShotViewController.getTargetView().getFlagView().tap);
            // buttons : vue secondaire, partie supérieure de main
            buttons = new UIView(new RectangleF(0, 0, main.Frame.Width, 100));
            buttons.BackgroundColor = UIColor.LightGray;
            // Définition du bouton enregistrer
            enregistrer = new UIButton(UIButtonType.RoundedRect);
            enregistrer.SetTitle("Oui", UIControlState.Normal);
			enregistrer.Frame = new RectangleF(main.Frame.Width - 75, 60, 75, 40);
            enregistrer.BackgroundColor = UIColor.White;
			// Définition du bouton surLeGreen
			surLeGreen = new UIButton(UIButtonType.RoundedRect);
			surLeGreen.SetTitle("Sur le green", UIControlState.Normal);
			surLeGreen.Frame = new RectangleF(main.Frame.Width - 100, main.Frame.Height-60, 100, 40);
			surLeGreen.BackgroundColor = UIColor.White;
            // Définition du bouton nePasEnregistrer
            nePasEnregistrer = new UIButton(UIButtonType.RoundedRect);
            nePasEnregistrer.SetTitle("Non", UIControlState.Normal);
            nePasEnregistrer.Frame = new RectangleF(main.Frame.Width - 160, 60, 75, 40);
            nePasEnregistrer.BackgroundColor = UIColor.White;
            // Définition de la textview
            var textView = new UILabel(new RectangleF(0, 60, main.Frame.Width - 160, 40));
            textView.Text = "Voulez-vous enregistrer ce coup ?";
            textView.Font = UIFont.FromName("Helvetica-Bold", 12f);
            textView.BackgroundColor = UIColor.White.ColorWithAlpha(0.4f);
            textView.LineBreakMode = UILineBreakMode.WordWrap;
            textView.TextAlignment = UITextAlignment.Center;
            textView.Lines = 0;
            #endregion

            #region Opérations sur les vues
            // Ajout de la vue (bouton) accepter dans la vue buttons
            buttons.AddSubview(enregistrer);
            buttons.AddSubview(nePasEnregistrer);
            buttons.AddSubview(textView);
            // Ajout de la vue buttons dans la vue main
            main.AddSubview(buttons);
            // Ajout de la vue map dans la vue main
            main.AddSubview(map);
			main.AddSubview(surLeGreen);
            // On déclare notre vue main comme étant l'attribut View de notre TargetViewController
            View = main;
            #endregion


            #region Définition de la map
            // Initialisation de la map
            // La position parDefaut est la position du drapeau (peu d'importance)
            Position parDefaut = new Position();
            gmap.init(parDefaut, true, true);
            // Définition du centre de la map, de la puissance du zoom, de la region
            // Le centre de la map est la position de l'objectif
            CLLocationCoordinate2D mapCenter = new CLLocationCoordinate2D(this.preShotViewController.getTargetView().target.Latitude, this.preShotViewController.getTargetView().target.Longitude);
            MKCoordinateRegion mapRegion = MKCoordinateRegion.FromDistance(mapCenter, 100, 100);
            // Permet de régler le zoom, diminuer la valeur pour augmenter celui ci
            mapRegion.Span.LatitudeDelta = 0.005;
            mapRegion.Span.LongitudeDelta = 0.005;
            map.CenterCoordinate = mapCenter;
            map.Region = mapRegion;
            #endregion
			// Etape du manager
			manager.etapeEnCours = Manager.Etape.resumeTir;
        }


        // Méthode après chargement de la vue
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();


            #region Replacement des marqueurs
            // Ajout d'un marqueur à l'emplacement du joueur (emplacement du coup joué)
            Position shot = new Position(map.UserLocation.Coordinate.Latitude, map.UserLocation.Coordinate.Longitude);
            shotmarker = gmap.ajouterMarqueur(shot, true, new MyColor(5, 10, 2, 0));

            #endregion

            #region Actions clicks
			surLeGreen.TouchDown += (sender, e) =>
			{
				greenViewController = new GreenViewController();
				greenViewController.manager = this.manager;
				this.NavigationController.PushViewController(greenViewController, true);
			};
            // Action lors d'un click sur le bouton accepter
            enregistrer.TouchDown += (sender, e) =>
            {
                // Ici, on doit passer au coup suivant
                // Et afficher une nouvelle targetView
                // On crée notre window, qui aura comme taille la taille de l'écran entier
				// On enregistre le coup du joueur
				manager.position = shot;
				manager.ajouterArrivee();
				manager.enregistrerTir();

                targetViewController = new TargetViewController();
                targetViewController.setFlagView(this.preShotViewController.getTargetView().getFlagView());
				targetViewController.manager = this.manager;
                UIWindow window = new UIWindow(targetViewController.View.Bounds);

                var nextNavigationController = new UINavigationController();
                // On attribue à window son NavigationController
                window.RootViewController = nextNavigationController;

                nextNavigationController.PushViewController(targetViewController, true);
				// On affiche window
				window.MakeKeyAndVisible();
            };
            // Action lors d'un click sur le bouton accepter
            nePasEnregistrer.TouchDown += (sender, e) =>
            {
				targetViewController = new TargetViewController();
				targetViewController.setFlagView(this.preShotViewController.getTargetView().getFlagView());
				targetViewController.manager = this.manager;
				UIWindow window = new UIWindow(targetViewController.View.Bounds);

				var nextNavigationController = new UINavigationController();
				// On attribue à window son NavigationController
				window.RootViewController = nextNavigationController;

				nextNavigationController.PushViewController(targetViewController, true);
				// On affiche window
				window.MakeKeyAndVisible();
            };
            #endregion

        } // ViewDidLoad


    } // Class ShotViewController

} // Namespace GolfApp.iOS2
