﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;
using GolfApp.Core;
using GolfApp.iOS2;

namespace GolfApp.iOS2
{
	public partial class MainViewController : UIViewController
	{
		#region Déclarations
		public Manager manager;
		UIView main;
		UIButton game;
		UIButton stats;
		UIButton help;
		StartGameViewController startGameViewController;
		StatsViewController statsViewController;
		HelpViewController helpViewController;
		#endregion

		// Chargement de la vue
		public override void LoadView ()
		{
			#region Déclarations
			// main : vue principale, taille full screen
			main = new UIView(UIScreen.MainScreen.Bounds);
			// Déclaration des boutons et ajout de ceux ci dans la vue
			// Définitions des boutons
			game = new UIButton(UIButtonType.RoundedRect);
			game.SetTitle ("Démarrer une partie", UIControlState.Normal);
			game.Frame = new RectangleF (60, 65, 200, 50);
			game.BackgroundColor = UIColor.Black.ColorWithAlpha(0.5f);
			game.SetTitleColor(UIColor.White, UIControlState.Normal);

			stats = new UIButton(UIButtonType.RoundedRect);
			stats.SetTitle ("Mes statistiques", UIControlState.Normal);
			stats.Frame = new RectangleF (60, 165, 200, 50);
			stats.BackgroundColor = UIColor.Black.ColorWithAlpha(0.5f);
			stats.SetTitleColor(UIColor.White, UIControlState.Normal);

			help = new UIButton(UIButtonType.RoundedRect);
			help.SetTitle ("Aide", UIControlState.Normal);
			help.Frame = new RectangleF (60, 265, 200, 50);
			help.BackgroundColor = UIColor.Black.ColorWithAlpha(0.5f);
			help.SetTitleColor(UIColor.White, UIControlState.Normal);

			// Ajout de la vue (bouton) stats à la vue main
			main.AddSubview(stats);
			// Ajout de la vue (bouton) game à la vue main
			main.AddSubview(game);
			// Ajout de la vue (bouton) help à la vue main
			main.AddSubview(help);
			// On définit notre vue main comme étant l'attribut vue de notre MainViewController
			View = main;
			// On définit le fond d'écran du menu principal (dans Resources/Images)
			View.BackgroundColor = UIColor.FromPatternImage (UIImage.FromFile ("Images/FrontImage.jpg"));
			#endregion
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			// Méthode de base pour switcher entre les vues sur iOS
			// NavigationController déclaré dans l'AppDelegate
			// Action lors d'un click sur le bouton game
			game.TouchDown += (sender, e) => {
				if(startGameViewController == null)
				{ startGameViewController = new StartGameViewController(); }
				//---- push our mapview screen onto the navigation
				//controller and pass a true so it navigates
				startGameViewController.manager = manager;
				this.NavigationController.PushViewController(startGameViewController, true);
			};

			// Action lors d'un click sur le bouton stats
			stats.TouchDown += (sender, e) => {
				if(statsViewController == null)
				{ statsViewController = new StatsViewController(); }
				this.NavigationController.PushViewController(statsViewController, true);
			};

			// Action lors d'un click sur le bouton help
			help.TouchDown += (sender, e) => {
				if(helpViewController == null)
				{ helpViewController = new HelpViewController(); }
				this.NavigationController.PushViewController(helpViewController, true);
			};
		}

		// Ces deux méthodes permettent d'afficher une barre de navigation 
		// une fois que l'on se déplace depuis la page main
		public override void ViewWillAppear (bool animated) {
			base.ViewWillAppear (animated);
			this.NavigationController.SetNavigationBarHidden (true, animated);
		}
		public override void ViewWillDisappear (bool animated) {
			base.ViewWillDisappear (animated);
			this.NavigationController.SetNavigationBarHidden (false, animated);
		}

	}
}

