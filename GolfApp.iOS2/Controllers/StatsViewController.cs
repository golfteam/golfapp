﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using BarChart;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;
using GolfApp.iOS2;

namespace GolfApp.iOS2
{
	public partial class StatsViewController : UIViewController
	{
		#region Déclarations
		UIView main;
		UIButton distance;
		UIButton deviation;
		StatsDistanceViewController statsDistanceController;
		StatsDeviationViewController statsDeviationController;
		#endregion

		// Constructeur de notre StatsViewController 
		// Surtout utile pour ajouter un titre à la vue
		public StatsViewController () : base ("StatsViewController", null) {
			this.Title = "Statistiques";
		}

		// Chargement de la vue
		public override void LoadView ()
		{
			#region Déclarations
			// main : vue principale, taille full screen - barre de navigation
			main = new UIView(new RectangleF(0,60,UIScreen.MainScreen.Bounds.Width,UIScreen.MainScreen.Bounds.Height-60));
			// Déclaration des boutons et ajout de ceux ci dans la vue
			// Définitions des boutons
			distance = new UIButton(UIButtonType.RoundedRect);
			distance.SetTitle ("Distance de l'objectif", UIControlState.Normal);
			distance.Frame = new RectangleF (60, 125, 200, 50);
			distance.BackgroundColor = UIColor.Black.ColorWithAlpha(0.5f);
			distance.SetTitleColor(UIColor.White, UIControlState.Normal);

			deviation = new UIButton(UIButtonType.RoundedRect);
			deviation.SetTitle ("Deviation de l'objectif", UIControlState.Normal);
			deviation.Frame = new RectangleF (60, 185, 200, 50);
			deviation.BackgroundColor = UIColor.Black.ColorWithAlpha(0.5f);
			deviation.SetTitleColor(UIColor.White, UIControlState.Normal);

			// Ajout de la vue (bouton) distance à la vue main
			main.AddSubview(distance);
			// Ajout de la vue (bouton) deviation à la vue main
			main.AddSubview(deviation);
			// On définit le fond d'écran du menu principal (dans Resources/Images)
			main.BackgroundColor = UIColor.FromPatternImage (UIImage.FromFile ("Images/statsfront.jpg"));
			// On définit notre vue main comme étant l'attribut vue de notre MainViewController
			View = main;
			#endregion
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();


			// Méthode de base pour switcher entre les vues sur iOS
			// NavigationController déclaré dans l'AppDelegate
			// Action lors d'un click sur le bouton distance
			distance.TouchDown += (sender, e) => {
				if(statsDistanceController == null)
				{ statsDistanceController = new StatsDistanceViewController(); }
				//---- push our mapview screen onto the navigation
				//controller and pass a true so it navigates
				this.NavigationController.PushViewController(statsDistanceController, true);
			};

			// Action lors d'un click sur le bouton deviation
			deviation.TouchDown += (sender, e) => {
				if(statsDeviationController == null)
				{ statsDeviationController = new StatsDeviationViewController(); }
				this.NavigationController.PushViewController(statsDeviationController, true);
			};
		}
			

	}
}

