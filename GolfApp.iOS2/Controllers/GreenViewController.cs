﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;
using GolfApp.Core;
using GolfApp.iOS2;
using GolfApp.iOS2.Impl;

namespace GolfApp.iOS2
{
	public partial class GreenViewController : UIViewController
    {
        #region Déclarations
        public Manager manager;
        UIView main;
		UIButton menu;
		UIButton nextHole;
        UITextField textBox;
		int putts;
        #endregion

        #region Méthodes publiques
        // Constructeur de notre TargetViewController 
		public GreenViewController()
			: base("GreenViewController", null)
        {
			this.Title = "Fin du trou";
        }
        #endregion

        // Chargement de la vue
        public override void LoadView()
        {
            // main : vue principale, taille full screen
            main = new UIView(UIScreen.MainScreen.Bounds);
            // Définition de la textview
            var textView = new UILabel(new RectangleF(0, 60, main.Frame.Width, 150));
			textView.Text = "Entrez le nombre de putts réalisés";
            textView.Font = UIFont.FromName("Helvetica-Bold", 20f);
            textView.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);
            textView.LineBreakMode = UILineBreakMode.WordWrap;
            textView.TextAlignment = UITextAlignment.Center;
            textView.Lines = 0;
            // Définition du textfield pour que l'utilisateur définisse le par
            var frame = new RectangleF(main.Frame.Width / 4, 210, main.Frame.Width / 2, 40);
            textBox = new UITextField(frame);
			textBox.Placeholder = "Nombre de putts";
            textBox.TextAlignment = UITextAlignment.Center;
            textBox.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);
            textBox.ReturnKeyType = UIReturnKeyType.Go;
            textBox.KeyboardType = UIKeyboardType.NumberPad;
			// Définition du bouton menu
			menu = new UIButton(UIButtonType.RoundedRect);
			menu.SetTitle("Retour au menu", UIControlState.Normal);
			menu.Frame = new RectangleF (60, 270, 200, 50);
			menu.BackgroundColor = UIColor.White;
			// Définition du bouton nextHole
			nextHole = new UIButton(UIButtonType.RoundedRect);
			nextHole.SetTitle("Trou suivant", UIControlState.Normal);
			nextHole.Frame = new RectangleF (60, 370, 200, 50);
			nextHole.BackgroundColor = UIColor.White;
            // On ajoute les subview à la vue main
            main.AddSubview(textBox);
            main.AddSubview(textView);
			main.AddSubview(menu);
			main.AddSubview (nextHole);
            View = main;
            // On définit le fond d'écran (dans Resources/Images) et l'image fleche
            View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("Images/FrontImage.jpg"));
			// Etape du manager
			manager.etapeEnCours = Manager.Etape.finTrou;

        }

        public override void ViewDidLoad()
        {
			menu.TouchDown += (sender, e) =>
            {
				UIWindow window = new UIWindow(this.View.Bounds);
				MainViewController mainViewController = new MainViewController();
				mainViewController.manager = this.manager;
				var nextNavigationController = new UINavigationController();
				// On attribue à window son NavigationController
				window.RootViewController = nextNavigationController;

				nextNavigationController.PushViewController(mainViewController, true);

				// On reset le nombre de coup
				TargetViewController.nbCoup = 1;
				// On affiche window
				window.MakeKeyAndVisible();
            };

			nextHole.TouchDown += (sender, e) =>
			{
				UIWindow window = new UIWindow(this.View.Bounds);
				FlagViewController flagViewController = new FlagViewController();
				flagViewController.manager = this.manager;
				var nextNavigationController = new UINavigationController();
				// On attribue à window son NavigationController
				window.RootViewController = nextNavigationController;

				nextNavigationController.PushViewController(flagViewController, true);

				// Reset du nombre de coup
				TargetViewController.nbCoup = 1;
				// On affiche window
				window.MakeKeyAndVisible();
			};

            // Permet de retirer le textfield de la vue une fois une valeur attribué au par
            // (On désactive le clavier lors d'un tap en dehors du clavier) 
            UITapGestureRecognizer tap = new UITapGestureRecognizer(g =>
            {
                try
                {
                    putts = int.Parse(textBox.Text);
                    this.View.EndEditing(true);
                }
                catch (System.FormatException)
                {
                    putts = 0;
                    this.View.EndEditing(true);
                }

            });
            this.View.AddGestureRecognizer(tap);
        }
    }
}
