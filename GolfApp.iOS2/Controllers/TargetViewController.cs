﻿using Core.BL.Geolocalisation;
using GolfApp.Core;
using GolfApp.Core.BL;
using GolfApp.iOS2.Impl;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace GolfApp.iOS2
{
    public partial class TargetViewController : UIViewController
    {
        #region Déclarations
		public Manager manager;
        UIView main;
        UIView buttons;
        public GoogleMapImpl gmap;
        public MKMapView map;
        public UITapGestureRecognizer tap;
        public Position target;
        UIButton club;
        UIButton jouer;
        Club selectedClub;
        FlagViewController flagViewController;
        PreShotViewController preShotViewController;
        public Position joueur;
		UILabel textView;
		UILabel distanceView;
        IList<string> clubs = new List<string>
		{
			"Driver",
			"Bois 3",
			"Bois 5",
			"Fer 3",
			"Fer 4",
			"Fer 5",
			"Fer 6",
			"Fer 7",
			"Fer 8",
			"Fer 9",
			"Pitch",
			"Sandwedge"
		};
        public static int nbCoup = 1;
        #endregion

        #region Méthodes publiques
        // Constructeur de notre TargetViewController 
        public TargetViewController()
            : base("TargetViewController", null)
        {
            this.Title = "Placer l'objectif";
        }

        // Set un FlagViewController à l'attribut de notre TargetViewController
        // Utile pour récupérer les données de la précédente mapView
        public void setFlagView(FlagViewController flagview)
        {
            this.flagViewController = flagview;
        }

        // Get l'attribut FlagViewController de notre TargetViewController
        public FlagViewController getFlagView()
        {
            return this.flagViewController;
        }
			
        #endregion

        // Chargement de la vue
        public override void LoadView()
        {
            #region Déclarations
            // main : vue principale, taille full screen
            main = new UIView(UIScreen.MainScreen.Bounds);
            // Initialisation de la google map
			gmap = (GoogleMapImpl) manager.map;
			map = gmap.map;
            // buttons : vue secondaire, partie supérieure de main
            buttons = new UIView(new RectangleF(0, 0, main.Frame.Width, 100));
            buttons.BackgroundColor = UIColor.LightGray;
            // Définition du bouton club
            club = new UIButton(UIButtonType.RoundedRect);
            club.SetTitle("Club", UIControlState.Normal);
            club.Frame = new RectangleF(0, 60, 75, 40);
            club.BackgroundColor = UIColor.White;
            // Définition du bouton jouer
            jouer = new UIButton(UIButtonType.RoundedRect);
            jouer.SetTitle("Jouer", UIControlState.Normal);
            jouer.Frame = new RectangleF(main.Frame.Width - 75, 60, 75, 40);
            jouer.BackgroundColor = UIColor.White;
            // Définition de la textview affichant le nb de coup
            textView = new UILabel(new RectangleF(30, 60, main.Frame.Width - 75, 40));
            textView.Text = "Coup n°" + nbCoup;
            textView.Font = UIFont.FromName("Helvetica-Bold", 16f);
            textView.BackgroundColor = UIColor.White.ColorWithAlpha(0.4f);
            textView.LineBreakMode = UILineBreakMode.WordWrap;
            textView.TextAlignment = UITextAlignment.Center;
            textView.Lines = 0;
			// Définition de la textview affichant les distances
			distanceView = new UILabel(new RectangleF(main.Frame.Width - 130, main.Frame.Height-40, 130, 40));
			distanceView.Font = UIFont.FromName("Helvetica-Bold", 16f);
			distanceView.LineBreakMode = UILineBreakMode.WordWrap;
			distanceView.BackgroundColor = UIColor.White.ColorWithAlpha(0.5f);
			distanceView.Lines = 0;
            #endregion

            #region Opérations sur les vues
            // Ajout de la vue (bouton) club, de la textview et de la vue (bouton) ok dans la vue buttons
            buttons.AddSubview(club);
            buttons.AddSubview(jouer);
            buttons.AddSubview(textView);
            // Ajout de la vue buttons dans la vue main
            main.AddSubview(buttons);
            // Ajout de la vue map dans la vue main
            main.AddSubview(map);
			main.AddSubview(distanceView);
            // On déclare notre vue main comme étant l'attribut View de notre TargetViewController
            View = main;
            #endregion


            
			// Etape du manager
			manager.etapeEnCours = Manager.Etape.defineObjective;
        }


        // Méthode après chargement de la vue
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

			#region Définition de la map
			// Définition du centre de la map, de la puissance du zoom, de la region
			// Le centre de la map est la position de l'utilisateur
			CLLocationCoordinate2D mapCenter = map.UserLocation.Coordinate;
			MKCoordinateRegion mapRegion = MKCoordinateRegion.FromDistance(mapCenter, 100, 100);
			// Permet de régler le zoom, diminuer la valeur pour augmenter celui ci
			mapRegion.Span.LatitudeDelta = 0.003;
			mapRegion.Span.LongitudeDelta = 0.003;
			map.CenterCoordinate = mapCenter;
			map.Region = mapRegion;
			// On définit l'attribut joueur (position du joueur sur la map)
			// Sera utile pour placer la position du joueur une fois la vue chargée
			joueur = new Position(mapCenter.Latitude, mapCenter.Longitude);
			// On centre la map sur l'utilisateur à chaque mise à jour de sa position
			// Et on met à jour la position du joueur dans le manager
			map.DidUpdateUserLocation += (sender, e) =>
			{
				if (map.UserLocation != null)
				{
					mapCenter = map.UserLocation.Coordinate;
					map.CenterCoordinate = mapCenter;
				}
				joueur = new Position(map.UserLocation.Coordinate.Latitude, map.UserLocation.Coordinate.Longitude);
				manager.OnLocationChanged(joueur);
			};
			#endregion


            #region Actions clicks
            // Action lors d'un click sur la map
            // Définition du marqueur objectif
            tap = new UITapGestureRecognizer(g =>
            {
                var pt = g.LocationInView(map);
                CLLocationCoordinate2D tapCoord = map.ConvertPoint(pt, map);
                target = new Position(tapCoord.Latitude, tapCoord.Longitude);
                // Pour n'avoir qu'un seul marqueur objectif, si celui ci existe déjà, on le supprime
                // avant de le replacer
				manager.onMapClick(target);
					Tir tir = new Tir();
					tir.depart = joueur;
					tir.objectif = target;
					distanceView.Text = "Objectif à "+Math.Truncate(tir.distanceDepartObjectif())+"m";
            });
            map.AddGestureRecognizer(tap);

            // Action lors d'un click sur le bouton Club
            // Ouverture d'un pickerview
            club.TouchDown += (sender, e) =>
            {
                this.SetupPicker();
            };

            // Action lors d'un click sur le bouton Jouer
            jouer.TouchDown += (sender, e) =>
            {
				if (!manager.isObjectiveDefined())
                {
                    UIAlertView nomarker = new UIAlertView()
                    {
                        Title = "Pas d'objectif !",
                        Message = "Veuillez placer un objectif avant de jouer"
                    };
                    nomarker.AddButton("OK");
                    nomarker.Show();
                }
				else if (manager.isObjectiveDefined())
                {
                    if (preShotViewController == null)
                    { preShotViewController = new PreShotViewController(); }
                    //---- push our mapview screen onto the navigation
                    //controller and pass a true so it navigates
                    preShotViewController.setTargetView(this);
                    this.NavigationController.PushViewController(preShotViewController, true);
                }
            };
            #endregion

        } // ViewDidLoad

        #region Méthodes privées
        private void SetupPicker()
        {
            // Setup the picker and model
            PickerModel model = new PickerModel(this.clubs);
            // Action lors d'une modification du picker
            // On définit l'attribut selectedClub en fonction du choix du joueur
            model.PickerChanged += (sender, e) =>
            {
				selectedClub = new Club(manager.ferStatChoisi(e.SelectedValue));
				manager.ferChoisi(e.SelectedValue);
                // On modifie le titre du bouton club
				club.SetTitle(e.SelectedValue, UIControlState.Normal);
            };

            UIPickerView picker = new UIPickerView();
            picker.ShowSelectionIndicator = true;
            picker.Model = model;

            // pickergroupview regroupe le picker et le bouton doneButton
            UIView pickergroupview = new UIView(new RectangleF(0, main.Frame.Height - picker.Frame.Height - 40, main.Frame.Width, picker.Frame.Height + 40));
            pickergroupview.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);

            // On créé donebutton, qui validera le choix du joueur
            UIButton doneButton = new UIButton(UIButtonType.RoundedRect);
            doneButton.Frame = new RectangleF(0, picker.Frame.Height, 75, 40);
            doneButton.SetTitle("OK", UIControlState.Normal);
            doneButton.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);

            // Action lors du click sur doneButton
            // On ferme le pickergroupview et on trace le cercle de rayon égal à la distance réalisé par le joueur avec le club sélectionné
            doneButton.TouchDown += (sender, e) =>
            {
                pickergroupview.Hidden = true;
                // Suppression du précédent cercle s'il existe (sinon empilement)
                if (gmap.circleOverlay != null)
                    gmap.retirerCercle(gmap.circleOverlay);
                gmap.ajouterCercle(new Position(joueur.Latitude, joueur.Longitude), selectedClub.getDefautDistance(), new MyColor(2, 10, 5, 0), 0, null);
            };

            // On ajoute les view à notre vue principal
            pickergroupview.AddSubview(doneButton);
            pickergroupview.AddSubview(picker);
            main.AddSubview(pickergroupview);
        }
        #endregion

    } // Class TargetViewController

} // Namespace GolfApp.iOS2
