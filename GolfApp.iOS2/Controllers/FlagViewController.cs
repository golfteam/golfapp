﻿using Core.BL.Geolocalisation;
using GolfApp.Core;
using GolfApp.Core.BL;
using GolfApp.iOS2.Impl;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using System.Drawing;

namespace GolfApp.iOS2
{
    public partial class FlagViewController : UIViewController
    {
        #region Déclarations
		public Manager manager;
        public UIView main;
        public UIView buttons;
        public GoogleMapImpl gmap;
        public MKMapView map;
        public GolfApp.Core.SAL.IPlatformDependantMapsMarker marker;
        public Position drapeau;
        public UIButton ok;
        public TargetViewController objectifViewController;
		public UITapGestureRecognizer tap;
        #endregion

        #region Méthode publiques
        // Constructeur de notre FlagViewController 
        // Le titre varie en fonction de l'étape à laquelle on se trouve (Drapeau ou Objectif)
        public FlagViewController()
            : base("FlagViewController", null)
        {
            this.Title = "Placer le drapeau";
        }

        // Get le marker en attribut
        public GolfApp.Core.SAL.IPlatformDependantMapsMarker getMarker()
        {
            return this.marker;
        }
        #endregion

        // Chargement de la vue
        public override void LoadView()
        {
            #region Déclarations
            // main : vue principale, taille full screen
            main = new UIView(UIScreen.MainScreen.Bounds);
            // Initialisation de la google map
			gmap = new GoogleMapImpl(0, 100, main.Frame.Width, main.Frame.Height - 100);
            map = gmap.map;
            map.Delegate = new MapDelegate();
			manager.startGame(gmap);
            // buttons : vue secondaire, partie supérieure de main
            buttons = new UIView(new RectangleF(0, 0, main.Frame.Width, 100));
            buttons.BackgroundColor = UIColor.LightGray;
            // Définition du bouton ok
            ok = new UIButton(UIButtonType.RoundedRect);
            ok.SetTitle("OK", UIControlState.Normal);
            ok.Frame = new RectangleF(main.Frame.Width - 75, 60, 75, 40);
            ok.BackgroundColor = UIColor.White;
            #endregion

            #region Opérations sur les vues
            // Ajout de la vue (bouton) ok dans la vue buttons
            buttons.AddSubview(ok);
            // Ajout de la vue buttons dans la vue main
            main.AddSubview(buttons);
            // Ajout de la vue map dans la vue main
            main.AddSubview(map);
            // On déclare notre vue main comme étant l'attribut View de notre FlagViewController
            View = main;
            #endregion
			// Etape du manager
			manager.etapeEnCours = Manager.Etape.defineFlag;
        }


        // Méthode après chargement de la vue
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            #region Définition de la map
            // Initialisation de la map
            Position parDefaut = new Position();
            gmap.init(parDefaut, true, true);
			// Définition du centre de la map (sur le joueur)
            CLLocationCoordinate2D mapCenter = map.UserLocation.Coordinate;
            map.CenterCoordinate = mapCenter;
            // On définit le zoom à effectuer
            // Diminuer les valeurs pour un zoom plus important
            MKCoordinateRegion mapRegion = MKCoordinateRegion.FromDistance(mapCenter, 100, 100);
            mapRegion.Span.LatitudeDelta = 0.003;
            mapRegion.Span.LongitudeDelta = 0.003;
            map.Region = mapRegion;
            // On centre la map sur l'utilisateur à chaque mise à jour de sa position
			// Et on met à jour la position du joueur dans le manager
            map.DidUpdateUserLocation += (sender, e) =>
            {
                if (map.UserLocation != null)
                {
                    mapCenter = map.UserLocation.Coordinate;
                    map.CenterCoordinate = mapCenter;
                }
				Position pos = new Position(map.UserLocation.Coordinate.Latitude, map.UserLocation.Coordinate.Longitude);
				manager.OnLocationChanged(pos);
            };
            #endregion


            #region Actions clicks
            // Action lors d'un click sur la map
			tap = new UITapGestureRecognizer(g =>
            {
                var pt = g.LocationInView(map);
                CLLocationCoordinate2D tapCoord = map.ConvertPoint(pt, map);
                drapeau = new Position(tapCoord.Latitude, tapCoord.Longitude);
                if (marker != null)
                {
                    gmap.retirerMarqueur(marker);
                }
                // Marqueur vert
                MyColor couleur = new MyColor(5, 10, 2, 0);
                marker = gmap.ajouterMarqueur(drapeau, false, couleur);
            });

            map.AddGestureRecognizer(tap);

            // Action lors d'un click sur le bouton OK
            // Vérification de la présence du marqueur sur la map
            // On push un nouveau TargetViewController pour placer l'objectif
            ok.TouchDown += (sender, e) =>
            {
                if (marker != null)
                {
                    if (objectifViewController == null)
                    { objectifViewController = new TargetViewController(); }
                    //---- push our mapview screen onto the navigation
                    //controller and pass a true so it navigates
                    objectifViewController.setFlagView(this);
					objectifViewController.manager = manager;
                    this.NavigationController.PushViewController(objectifViewController, true);
                }
                else if (marker == null)
                {
                    UIAlertView nomarker = new UIAlertView()
                    {
                        Title = "Pas de drapeau !",
                        Message = "Veuillez placer le drapeau avant de définir votre objectif"
                    };
                    nomarker.AddButton("OK");
                    nomarker.Show();
                }
            };
            #endregion

        } // ViewDidLoad

    } // Class FlagViewController

} // Namespace GolfApp.iOS2
