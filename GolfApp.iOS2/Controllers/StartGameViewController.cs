﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using GolfApp.Core.BL;
using GolfApp.Core.SAL;
using GolfApp.Core;
using GolfApp.iOS2;
using GolfApp.iOS2.Impl;

namespace GolfApp.iOS2
{
    public partial class StartGameViewController : UIViewController
    {
        #region Déclarations
        public Manager manager;
        UIView main;
        UIButton valider;
        FlagViewController flagViewController;
        UITextField textBox;
        int par;
        #endregion

        #region Méthodes publiques
        // Constructeur de notre TargetViewController 
        public StartGameViewController()
            : base("StartGameViewController", null)
        {
            this.Title = "Définir le par";
        }
        #endregion

        // Chargement de la vue
        public override void LoadView()
        {
            // main : vue principale, taille full screen
            main = new UIView(UIScreen.MainScreen.Bounds);
            // Définition de la textview
            var textView = new UILabel(new RectangleF(0, 60, main.Frame.Width, 150));
            textView.Text = "Définissez le par du trou à jouer";
            textView.Font = UIFont.FromName("Helvetica-Bold", 20f);
            textView.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);
            textView.LineBreakMode = UILineBreakMode.WordWrap;
            textView.TextAlignment = UITextAlignment.Center;
            textView.Lines = 0;
            // Définition du textfield pour que l'utilisateur définisse le par
            var frame = new RectangleF(main.Frame.Width / 4, 210, main.Frame.Width / 2, 40);
            textBox = new UITextField(frame);
            textBox.Placeholder = "Entrez le par ici";
            textBox.TextAlignment = UITextAlignment.Center;
            textBox.BackgroundColor = UIColor.White.ColorWithAlpha(0.8f);
            textBox.ReturnKeyType = UIReturnKeyType.Go;
            textBox.KeyboardType = UIKeyboardType.NumberPad;
            // Définition du bouton valider
            valider = new UIButton(UIButtonType.RoundedRect);
            valider.SetTitle("OK", UIControlState.Normal);
            valider.Frame = new RectangleF(main.Frame.Width - 100, main.Frame.Height - 50, 90, 40);
            valider.BackgroundColor = UIColor.White;
            // Déc
            // On ajoute les subview à la vue main
            main.AddSubview(textBox);
            main.AddSubview(textView);
            main.AddSubview(valider);
            View = main;
            // On définit le fond d'écran (dans Resources/Images) et l'image fleche
            View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("Images/FrontImage.jpg"));
			// Etape du manager
			manager.etapeEnCours = Manager.Etape.startGame;

        }

        public override void ViewDidLoad()
        {
            valider.TouchDown += (sender, e) =>
            {
                if (par == 0)
                {
                    UIAlertView nopar = new UIAlertView()
                    {
                        Title = "Par non défini",
                        Message = "Veuillez définir le par du trou avant de commencer à jouer"
                    };
                    nopar.AddButton("OK");
                    nopar.Show();
                }
                else if (par != 0)
                {
                    manager.par = par;
                    if (flagViewController == null)
                    { flagViewController = new FlagViewController(); }
                    //---- push our mapview screen onto the navigation
                    //controller and pass a true so it navigates
					flagViewController.manager = manager;
                    this.NavigationController.PushViewController(flagViewController, true);
                }
            };

            // Permet de retirer le textfield de la vue une fois une valeur attribué au par
            // (On désactive le clavier lors d'un tap en dehors du clavier) 
            UITapGestureRecognizer tap = new UITapGestureRecognizer(g =>
            {
                try
                {
                    par = int.Parse(textBox.Text);
                    this.View.EndEditing(true);
                }
                catch (System.FormatException)
                {
                    par = 0;
                    this.View.EndEditing(true);
                }

            });
            this.View.AddGestureRecognizer(tap);
        }
    }
}
