﻿using GolfApp.Core.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PCLStorage;
using System.Threading.Tasks;

namespace GolfApp.UnitTests
{
    [TestClass]
    public class TestStats
    {
        Stats stats = new Stats();
        IFolder dataFolder;
        [TestMethod]
        public void TestStatDeviation()
        {
            Task<IFolder> task1 = FileSystem.Current.GetFolderFromPathAsync("C:\\Users\\Mathias\\Documents\\INSA\\3A\\EP");
            task1.Wait();

            IFolder rootFolder = task1.Result;
            Task<IFolder> dataFolderTask = rootFolder.CreateFolderAsync("data", CreationCollisionOption.OpenIfExists);
            dataFolderTask.Wait();

            dataFolder = dataFolderTask.Result;

            float[] res = stats.getStatsDeviation(dataFolder, Club.typeClub.fer_6);

        }
    }
}
