﻿using Core.BL.Geolocalisation;
using GolfApp.Core.SAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace GolfApp.UnitTests
{
    [TestClass]
    public class TestWeather
    {
        [TestMethod]
        public void TestMethod1()
        {
            Position l = new Position();
            l.Latitude = 48.1134750;
            l.Longitude = -1.6757080;
            l.Accuracy = 0;
            Weather w = new Weather(l);
            Task t = w.init();
            /// On fait autre chose
            t.Wait();
        }
    }
}
