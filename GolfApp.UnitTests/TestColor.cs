﻿using System;
using GolfApp.Core.BL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GolfApp.Core.SAL;

namespace GolfApp.UnitTests
{
    [TestClass]
    public class TestColor
    {
        [TestMethod]
        public void TestARGB()
        {
            MyColor c = new MyColor(1, 1, 1, 1);

            Assert.AreEqual<Int32>(0x01010101, c.toARGB());
        }
    }
}
